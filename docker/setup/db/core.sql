DROP TABLE IF EXISTS sa_modules;

CREATE TABLE sa_modules
(
	mod_id   INT         NOT NULL AUTO_INCREMENT,
	mod_name VARCHAR(45) NOT NULL,
	mod_ver  VARCHAR(45) NOT NULL,
	mod_desc TEXT,
	PRIMARY KEY ( mod_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sa_sitevars;

CREATE TABLE sa_sitevars
(
	sv_id    BIGINT            NOT NULL AUTO_INCREMENT,
	sv_type  SMALLINT UNSIGNED NOT NULL DEFAULT '0',
	sv_var   VARCHAR(200)      NOT NULL,
	sv_value VARCHAR(200)      NOT NULL,
	PRIMARY KEY ( sv_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sa_table;

CREATE TABLE sa_table
(
	t_id     INT UNSIGNED NOT NULL AUTO_INCREMENT,
	mod_id   INT UNSIGNED NOT NULL,
	t_dt_add TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
	t_dt_mod DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	t_name   VARCHAR(60)  NOT NULL,
	t_desc   TEXT,
	t_dbname VARCHAR(60)  NOT NULL,
	t_dbnick VARCHAR(10)  NOT NULL,
	t_dbtype VARCHAR(60)  NOT NULL DEFAULT 'InnoDB',
	PRIMARY KEY ( t_id )
)
	ENGINE = InnoDB;

#
DROP TABLE IF EXISTS sa_telem;

CREATE TABLE sa_telem
(
	te_id            INT UNSIGNED NOT NULL AUTO_INCREMENT,
	t_id             INT UNSIGNED NOT NULL,
	te_dt_add        DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
	te_dt_mod        DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	te_name          VARCHAR(60)  NOT NULL,
	te_desc          TEXT,
	te_pos           SMALLINT UNSIGNED     DEFAULT '0',
	te_db_name       VARCHAR(60)           DEFAULT NULL,
	te_db_type       VARCHAR(50)           DEFAULT NULL,
	te_db_typedesc   VARCHAR(50)           DEFAULT NULL,
	te_db_notnull    TINYINT UNSIGNED      DEFAULT '0',
	te_db_unsigned   TINYINT UNSIGNED      DEFAULT '0',
	te_db_autoinc    TINYINT UNSIGNED      DEFAULT '0',
	te_db_prikey     TINYINT UNSIGNED      DEFAULT '0',
	te_db_default    VARCHAR(50)           DEFAULT NULL,
	te_db_fulltext   TINYINT UNSIGNED      DEFAULT '0',
	te_d_nick        VARCHAR(50)           DEFAULT NULL,
	te_d_optional    TINYINT UNSIGNED      DEFAULT '0',
	te_d_special     TINYINT UNSIGNED      DEFAULT '0',
	te_d_parenttable INT UNSIGNED          DEFAULT '0',
	te_f_dtype       VARCHAR(50)           DEFAULT NULL,
	te_f_minl        SMALLINT UNSIGNED     DEFAULT '0',
	te_f_maxl        SMALLINT UNSIGNED     DEFAULT '0',
	te_f_maxli       SMALLINT UNSIGNED     DEFAULT '0',
	PRIMARY KEY ( te_id )
)
	ENGINE = InnoDB;

# |
DROP TABLE IF EXISTS sb_errorlog;

CREATE TABLE sb_errorlog
(
	elog_id      INT UNSIGNED     NOT NULL AUTO_INCREMENT,
	elog_dtadd   TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
	elog_type    TINYINT UNSIGNED NOT NULL,
	elog_key     VARCHAR(45)               DEFAULT NULL,
	elog_content TEXT,
	PRIMARY KEY ( elog_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sb_gtable;

CREATE TABLE sb_gtable
(
	gt_id     INT UNSIGNED NOT NULL AUTO_INCREMENT,
	t_id      INT UNSIGNED NOT NULL,
	gt_dt_add DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
	gt_dt_mod DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	gt_dbname VARCHAR(60)           DEFAULT NULL,
	gt_dbtype VARCHAR(60)  NOT NULL DEFAULT 'InnoDB',
	PRIMARY KEY ( gt_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sb_gtelem;

CREATE TABLE sb_gtelem
(
	gte_id          INT UNSIGNED NOT NULL AUTO_INCREMENT,
	gt_id           INT UNSIGNED NOT NULL,
	te_id           INT UNSIGNED NOT NULL,
	gte_db_name     VARCHAR(60)      DEFAULT NULL,
	gte_db_type     VARCHAR(50)      DEFAULT NULL,
	gte_db_typedesc VARCHAR(50)      DEFAULT NULL,
	gte_db_notnull  TINYINT UNSIGNED DEFAULT '0',
	gte_db_unsigned TINYINT UNSIGNED DEFAULT '0',
	gte_db_autoinc  TINYINT UNSIGNED DEFAULT '0',
	gte_db_prikey   TINYINT UNSIGNED DEFAULT '0',
	gte_db_default  VARCHAR(50)      DEFAULT NULL,
	gte_db_fulltext TINYINT UNSIGNED DEFAULT '0',
	PRIMARY KEY ( gte_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sb_notifications;

CREATE TABLE sb_notifications
(
	n_id               INT UNSIGNED     NOT NULL AUTO_INCREMENT,
	a_id               INT UNSIGNED              DEFAULT NULL,
	n_dt_add           TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
	n_dt_mod           TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	n_app              VARCHAR(10)      NOT NULL,
	n_obj              VARCHAR(10)      NOT NULL,
	n_action_type      VARCHAR(10)      NOT NULL,
	n_action_id        INT UNSIGNED     NOT NULL,
	n_object_id_action INT UNSIGNED     NOT NULL,
	n_status           TINYINT UNSIGNED NOT NULL,
	n_url              VARCHAR(100)     NOT NULL,
	n_title            VARCHAR(75)      NOT NULL,
	n_content          VARCHAR(240)     NOT NULL,
	PRIMARY KEY ( n_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sb_objects_obj;

CREATE TABLE sb_objects_obj
(
	a_id                   INT UNSIGNED     NOT NULL AUTO_INCREMENT,
	a_id_parent            INT UNSIGNED     NOT NULL DEFAULT '0',
	a_dt_add               TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
	a_dt_mod               TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	a_dt_lastactive        TIMESTAMP        NULL     DEFAULT NULL,
	a_ikey                 VARCHAR(32)               DEFAULT NULL,
	a_fname                VARCHAR(70)               DEFAULT NULL,
	a_lname                VARCHAR(70)               DEFAULT NULL,
	a_obj_name             VARCHAR(100)     NOT NULL,
	a_url_name_full        VARCHAR(100)     NOT NULL,
	a_url_name_simple      VARCHAR(100)     NOT NULL,
	a_img_full             VARCHAR(75)      NOT NULL DEFAULT 'default_img',
	a_img_crop             VARCHAR(75)      NOT NULL DEFAULT 'default_img',
	a_banner_full          VARCHAR(75)      NOT NULL DEFAULT 'default_banner',
	a_banner_crop          VARCHAR(75)      NOT NULL DEFAULT 'default_banner',
	a_type                 VARCHAR(10)      NOT NULL DEFAULT 'user',
	a_adm_level            TINYINT UNSIGNED NOT NULL DEFAULT '2',
	a_adm_type             VARCHAR(10)               DEFAULT NULL,
	a_status               TINYINT UNSIGNED NOT NULL DEFAULT '0',
	a_status_bloqued       TINYINT UNSIGNED NOT NULL DEFAULT '0',
	a_status_published     TINYINT UNSIGNED NOT NULL DEFAULT '1',
	a_status_active        TINYINT UNSIGNED NOT NULL DEFAULT '1',
	a_status_featured      TINYINT UNSIGNED NOT NULL DEFAULT '0',
	a_status_useradmin     TINYINT UNSIGNED NOT NULL DEFAULT '0',
	a_prop_physical        TINYINT UNSIGNED NOT NULL DEFAULT '0',
	a_notif_adm_nexttime   DATETIME                  DEFAULT NULL,
	a_notif_adm_hourperiod SMALLINT UNSIGNED         DEFAULT '8',
	a_notif_adm_mailstatus TINYINT UNSIGNED          DEFAULT '1',
	a_count_notif          TINYINT          NOT NULL DEFAULT '0',
	a_count_msgs           TINYINT          NOT NULL DEFAULT '0',
	a_count_members        INT UNSIGNED     NOT NULL DEFAULT '0',
	a_count_subscribers    INT UNSIGNED     NOT NULL DEFAULT '0',
	a_count_commends       INT UNSIGNED     NOT NULL DEFAULT '0',
	a_count_reviews        INT UNSIGNED     NOT NULL DEFAULT '0',
	a_validated            TINYINT          NOT NULL DEFAULT '0',
	a_geopoint_lat         DECIMAL(20,
		                       17)                   DEFAULT NULL,
	a_geopoint_lng         DECIMAL(20,
		                       17)                   DEFAULT NULL,
	a_geopoint_alt         DECIMAL(20,
		                       10)                   DEFAULT NULL,
	a_upass                CHAR(120)                 DEFAULT NULL,
	a_data_links           TINYTEXT,
	PRIMARY KEY ( a_id )
)
	ENGINE = InnoDB
	AUTO_INCREMENT = 2;

# |
# | DATOS 'sb_objects_obj'
# |
INSERT
INTO
	sb_objects_obj
VALUES
	('1',
	 '0',
	 NOW( ),
	 NOW( ),
	 NULL,
	 NULL,
	 'First Name',
	 'Last Name',
	 'Testing Name',
	 'testuser',
	 '',
	 'default_img',
	 'default_img',
	 'default_banner',
	 'default_banner',
	 'user',
	 '0',
	 '0',
	 '1',
	 '0',
	 '1',
	 '1',
	 '0',
	 '0',
	 '0',
	 NULL,
	 '8',
	 '1',
	 '0',
	 '0',
	 '0',
	 '0',
	 '0',
	 '0',
	 '0',
	 NULL,
	 NULL,
	 NULL,
	 '$1$lI5.KE..$wOxIs0KaGdLaYBhWJv13X1',
	 NULL);

DROP TABLE IF EXISTS sb_objects_properties;

CREATE TABLE sb_objects_properties
(
	op_id     INT UNSIGNED NOT NULL AUTO_INCREMENT,
	a_id      INT UNSIGNED NOT NULL,
	op_dt_add TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
	op_dt_mod TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	app_name  VARCHAR(10)  NOT NULL,
	app_type  VARCHAR(10)  NOT NULL,
	op_value  TEXT         NOT NULL,
	PRIMARY KEY ( op_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sb_objects_relationships;

CREATE TABLE sb_objects_relationships
(
	orel_id          BIGINT UNSIGNED  NOT NULL AUTO_INCREMENT,
	obj_id_target    INT UNSIGNED     NOT NULL,
	obj_id_from      INT UNSIGNED     NOT NULL,
	orel_dt_add      TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
	orel_dt_mod      TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	orel_status      TINYINT UNSIGNED NOT NULL DEFAULT '0',
	orel_blocked     TINYINT UNSIGNED NOT NULL DEFAULT '0',
	orel_subscribed  TINYINT UNSIGNED NOT NULL DEFAULT '0',
	orel_commend     TINYINT UNSIGNED NOT NULL DEFAULT '0',
	orel_admin_level TINYINT UNSIGNED NOT NULL DEFAULT '0',
	orel_admin_type  VARCHAR(10)               DEFAULT NULL,
	PRIMARY KEY ( orel_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sb_users_login_log;

CREATE TABLE sb_users_login_log
(
	log_id      BIGINT UNSIGNED   NOT NULL AUTO_INCREMENT,
	log_ip      VARCHAR(20)       NOT NULL,
	log_dtlast  DATETIME          NOT NULL,
	log_attemps SMALLINT UNSIGNED NOT NULL DEFAULT '0',
	a_id        INT UNSIGNED      NOT NULL DEFAULT '0',
	log_type    TINYINT UNSIGNED  NOT NULL DEFAULT '0',
	PRIMARY KEY ( log_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sb_users_logins;

CREATE TABLE sb_users_logins
(
	ul_id      INT UNSIGNED NOT NULL AUTO_INCREMENT,
	a_id       INT UNSIGNED NOT NULL DEFAULT '0',
	ul_ip      VARCHAR(45)  NOT NULL DEFAULT '0',
	ul_dtadd   DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
	ul_dtlast  DATETIME              DEFAULT NULL,
	ul_session CHAR(32)     NOT NULL DEFAULT '0',
	ul_cookie  CHAR(32)     NOT NULL DEFAULT '0',
	PRIMARY KEY ( ul_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sb_users_mail;

CREATE TABLE sb_users_mail
(
	am_id     INT UNSIGNED     NOT NULL AUTO_INCREMENT,
	a_id      INT UNSIGNED     NOT NULL,
	am_dt_add TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
	am_dt_mod TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	am_mail   VARCHAR(100)     NOT NULL,
	am_type   TINYINT UNSIGNED NOT NULL DEFAULT '1',
	am_status TINYINT UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY ( am_id )
)
	ENGINE = InnoDB
	AUTO_INCREMENT = 2;

# |
# | DATOS 'sb_users_mail'
# |
INSERT
INTO
	sb_users_mail
VALUES
	('1', '1', 'testuser@example.com', '1', '1', NOW( ), NOW( ));

DROP TABLE IF EXISTS sb_users_socnet;

CREATE TABLE sb_users_socnet
(
	asn_id         INT UNSIGNED NOT NULL AUTO_INCREMENT,
	a_id           INT UNSIGNED NOT NULL,
	asn_type       VARCHAR(3)   NOT NULL,
	asn_sn_id      VARCHAR(50)  NOT NULL,
	asn_o          VARBINARY(100)        DEFAULT NULL,
	asn_os         VARBINARY(100)        DEFAULT NULL,
	asn_dt_add     TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
	asn_last_login DATETIME              DEFAULT NULL,
	PRIMARY KEY ( asn_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sb_variables;

CREATE TABLE sb_variables
(
	var_id    INT UNSIGNED NOT NULL AUTO_INCREMENT,
	var_name  VARCHAR(20)  NOT NULL,
	var_value TEXT         NOT NULL,
	PRIMARY KEY ( var_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sc_site_articles;

CREATE TABLE sc_site_articles
(
	art_id                     INT UNSIGNED     NOT NULL AUTO_INCREMENT,
	a_id                       INT UNSIGNED     NOT NULL,
	cat_id                     SMALLINT UNSIGNED         DEFAULT NULL,
	img_id                     INT UNSIGNED     NOT NULL DEFAULT '0',
	art_id_parent              INT UNSIGNED     NOT NULL DEFAULT '0',
	com_id                     INT UNSIGNED     NOT NULL DEFAULT '0',
	art_dt_add                 TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
	art_dt_mod                 TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	art_dt_pub                 DATETIME                  DEFAULT NULL,
	art_status_published       TINYINT UNSIGNED NOT NULL DEFAULT '0',
	art_status_indexable       TINYINT UNSIGNED NOT NULL DEFAULT '0',
	art_type_primary           VARCHAR(10)               DEFAULT NULL,
	art_type_secondary         VARCHAR(10)               DEFAULT NULL,
	art_lang                   VARCHAR(10)               DEFAULT NULL,
	art_order_parent           TINYINT UNSIGNED          DEFAULT NULL,
	art_order_level            TINYINT                   DEFAULT NULL,
	art_order_current          VARCHAR(4)                DEFAULT NULL,
	art_order_hierarchy        VARCHAR(252)              DEFAULT NULL,
	art_url                    VARCHAR(250)              DEFAULT NULL,
	art_title                  VARCHAR(150)     NOT NULL,
	art_urltitle               VARCHAR(150)              DEFAULT NULL,
	art_meta_word              TINYTEXT,
	art_meta_desc              TINYTEXT,
	art_content                TEXT,
	art_content_extra          TEXT,
	art_content_extra_b        TINYTEXT,
	art_img_struct             VARCHAR(60)               DEFAULT NULL,
	art_count_views            INT UNSIGNED     NOT NULL DEFAULT '0',
	art_count_likes            INT UNSIGNED     NOT NULL DEFAULT '0',
	art_count_nolikes          INT UNSIGNED     NOT NULL DEFAULT '0',
	art_img_name_url           VARCHAR(60)               DEFAULT NULL,
	art_img_alt_text           VARCHAR(60)               DEFAULT NULL,
	art_img_width              SMALLINT                  DEFAULT NULL,
	art_img_heigth             SMALLINT                  DEFAULT NULL,
	art_content_bundles        VARCHAR(200)              DEFAULT NULL,
	art_content_header_type    VARCHAR(10)               DEFAULT NULL,
	art_content_header_content VARCHAR(100)              DEFAULT NULL,
	art_body_component         VARCHAR(50)               DEFAULT NULL,
	art_content_redirect       VARCHAR(200)              DEFAULT NULL,
	PRIMARY KEY ( art_id ),
	FULLTEXT KEY art_title ( art_title,
	                         art_meta_desc,
	                         art_content )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sc_site_banners;

CREATE TABLE sc_site_banners
(
	ban_id        INT UNSIGNED     NOT NULL AUTO_INCREMENT,
	ban_dt_add    TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
	ban_dt_mod    TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	ban_title     VARCHAR(200)     NOT NULL,
	ban_type      TINYINT UNSIGNED NOT NULL,
	ban_size      TINYINT UNSIGNED          DEFAULT NULL,
	ban_alt       VARCHAR(200)              DEFAULT NULL,
	ban_code      TINYTEXT,
	ban_url       VARCHAR(250)              DEFAULT NULL,
	ban_floc      VARCHAR(250)              DEFAULT NULL,
	ban_fsize     VARCHAR(20)               DEFAULT NULL,
	ban_ftype     VARCHAR(5)                DEFAULT NULL,
	ban_fname     VARCHAR(200)              DEFAULT NULL,
	ban_dtini     DATETIME                  DEFAULT NULL,
	ban_dtend     DATETIME                  DEFAULT NULL,
	ban_maxclicks INT UNSIGNED     NOT NULL DEFAULT '0',
	ban_curclicks INT UNSIGNED     NOT NULL DEFAULT '0',
	ban_maxprints INT UNSIGNED     NOT NULL DEFAULT '0',
	ban_curprints INT UNSIGNED     NOT NULL DEFAULT '0',
	ban_active    TINYINT UNSIGNED NOT NULL DEFAULT '0',
	ban_showed    TINYINT UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY ( ban_id )
)
	ENGINE = InnoDB;


DROP TABLE IF EXISTS sc_site_category;

CREATE TABLE sc_site_category
(
	cat_id        INT UNSIGNED NOT NULL AUTO_INCREMENT,
	cat_title     VARCHAR(100) NOT NULL,
	cat_urltitle  VARCHAR(100) DEFAULT NULL,
	cat_mode_type VARCHAR(10)  DEFAULT NULL,
	PRIMARY KEY ( cat_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sc_site_contact;

CREATE TABLE sc_site_contact
(
	uc_id             INT AUTO_INCREMENT PRIMARY KEY,
	uc_dt_add         TIMESTAMP        DEFAULT CURRENT_TIMESTAMP NOT NULL,
	uc_dt_mod         TIMESTAMP        DEFAULT CURRENT_TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
	uc_usr_uname      VARCHAR(100)                               NOT NULL,
	uc_usr_email      VARCHAR(100)                               NOT NULL,
	uc_usr_message    TEXT                                       NULL,
	uc_usr_metadata   TEXT                                       NULL,
	uc_usr_reference  VARCHAR(200)                               NULL,
	uc_usr_ip         VARBINARY(16)                              NOT NULL,
	uc_usr_useragent  VARCHAR(250)                               NULL,
	uc_status_revised TINYINT UNSIGNED DEFAULT '0'               NOT NULL,
	uc_status_spam    TINYINT UNSIGNED DEFAULT '0'               NOT NULL
);

DROP TABLE IF EXISTS sc_site_diagrams;

CREATE TABLE sc_site_diagrams
(
	d_id        INT AUTO_INCREMENT
		PRIMARY KEY,
	a_id        INT                                 NOT NULL,
	d_mode_type VARCHAR(10)                         NULL,
	d_dt_add    TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	d_dt_mod    TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	d_type      VARCHAR(10)                         NULL,
	d_title     VARCHAR(100)                        NULL,
	d_content   TEXT                                NULL,
	d_file_name VARCHAR(120)                        NULL
);



DROP TABLE IF EXISTS sc_site_documents;

CREATE TABLE sc_site_documents
(
	docs_id               INT UNSIGNED     NOT NULL AUTO_INCREMENT,
	img_id                INT UNSIGNED     NOT NULL DEFAULT '0',
	cat_id                INT UNSIGNED     NOT NULL,
	author_id             INT UNSIGNED              DEFAULT '0',
	docs_dt_add           TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
	docs_dt_mod           TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	docs_title_normal     VARCHAR(50)      NOT NULL,
	docs_title_url        VARCHAR(50)      NOT NULL,
	docs_description      TINYTEXT,
	docs_content          TEXT,
	docs_img_struct       VARCHAR(60)               DEFAULT NULL,
	docs_published        TINYINT UNSIGNED NOT NULL DEFAULT '0',
	docs_access_level     TINYINT UNSIGNED NOT NULL DEFAULT '0',
	docs_access_link_req  TINYINT UNSIGNED NOT NULL DEFAULT '0',
	docs_mode_type        VARCHAR(10)      NOT NULL DEFAULT 'normal',
	docs_mode_id          INT UNSIGNED              DEFAULT NULL,
	docs_img_name_url     VARCHAR(60)               DEFAULT NULL,
	docs_img_alt_text     VARCHAR(60)               DEFAULT NULL,
	docs_content_bundles  VARCHAR(200)              DEFAULT NULL,
	docs_content_body_mod VARCHAR(50)               DEFAULT NULL,
	PRIMARY KEY ( docs_id )
)
	ENGINE = InnoDB;


DROP TABLE IF EXISTS sc_site_documents_access;

CREATE TABLE sc_site_documents_access
(
	dacc_id        INT UNSIGNED     NOT NULL AUTO_INCREMENT,
	doc_id         INT UNSIGNED     NOT NULL,
	user_id        INT UNSIGNED     NOT NULL,
	dacc_dt_add    TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dacc_dt_mod    TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	dacc_last_page INT UNSIGNED     NOT NULL DEFAULT '0',
	dacc_status    TINYINT UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY ( dacc_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sc_site_files;

CREATE TABLE sc_site_files
(
	file_id        INT UNSIGNED NOT NULL AUTO_INCREMENT,
	obj_id         INT UNSIGNED NOT NULL DEFAULT '0',
	file_dt_add    TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
	file_dt_mod    TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	file_title     VARCHAR(100) NOT NULL,
	file_desc      TINYTEXT,
	file_ftype     VARCHAR(5)   NOT NULL,
	file_floc      TINYTEXT     NOT NULL,
	file_fname     TINYTEXT     NOT NULL,
	file_fsize     TINYTEXT     NOT NULL,
	file_mode_type VARCHAR(10)           DEFAULT 'normal',
	file_mode_id   INT UNSIGNED          DEFAULT NULL,
	PRIMARY KEY ( file_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sc_site_gallery;

CREATE TABLE sc_site_gallery
(
	gal_id          MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
	gal_dt_add      TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP,
	gal_dt_mod      TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	gal_title       VARCHAR(50)        NOT NULL,
	gal_description TINYTEXT,
	gal_structure   TEXT,
	gal_ammount     TINYINT UNSIGNED   NOT NULL DEFAULT '0',
	PRIMARY KEY ( gal_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sc_site_gallery_elements;

CREATE TABLE sc_site_gallery_elements
(
	gel_id          INT UNSIGNED       NOT NULL AUTO_INCREMENT,
	gal_id          MEDIUMINT UNSIGNED NOT NULL,
	img_id          INT UNSIGNED       NOT NULL,
	gel_title       VARCHAR(50)                 DEFAULT NULL,
	gel_description TINYTEXT,
	gel_order       TINYINT UNSIGNED   NOT NULL DEFAULT '0',
	PRIMARY KEY ( gel_id ),
	KEY parenttable ( gal_id )
)
	ENGINE = InnoDB;


DROP TABLE IF EXISTS sc_site_images;

CREATE TABLE sc_site_images
(
	img_id              INT UNSIGNED      NOT NULL AUTO_INCREMENT,
	sit_id              SMALLINT UNSIGNED NOT NULL,
	obj_id              INT UNSIGNED      NOT NULL,
	img_dt_add          TIMESTAMP         NOT NULL DEFAULT CURRENT_TIMESTAMP,
	img_dt_mod          TIMESTAMP         NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	img_name            VARCHAR(60)       NOT NULL,
	img_desc            TINYTEXT,
	img_fname           VARCHAR(100)      NOT NULL,
	img_ftype           VARCHAR(5)        NOT NULL,
	img_floc            VARCHAR(250)      NOT NULL,
	img_fsize           VARCHAR(100)      NOT NULL,
	img_width           SMALLINT UNSIGNED NOT NULL DEFAULT '0',
	img_height          SMALLINT UNSIGNED NOT NULL DEFAULT '0',
	img_fstruct         VARCHAR(100)      NOT NULL,
	img_mode_type       VARCHAR(10)       NOT NULL DEFAULT 'normal',
	img_mode_id         INT UNSIGNED               DEFAULT NULL,
	img_name_url        VARCHAR(60)                DEFAULT NULL,
	img_code            CHAR(8)           NOT NULL,
	img_prop_adjustable TINYINT(1)                 DEFAULT '1',
	PRIMARY KEY ( img_id ),
	FULLTEXT KEY img_name ( img_name,
	                        img_desc )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sc_site_tagrel;

CREATE TABLE sc_site_tagrel
(
	tagr_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	tag_id  INT UNSIGNED NOT NULL,
	art_id  INT UNSIGNED NOT NULL,
	PRIMARY KEY ( tagr_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sc_site_tags;

CREATE TABLE sc_site_tags
(
	tag_id        INT UNSIGNED NOT NULL AUTO_INCREMENT,
	tag_title     VARCHAR(100) NOT NULL,
	tag_urltitle  VARCHAR(100) DEFAULT NULL,
	tag_mode_type VARCHAR(10)  DEFAULT NULL,
	PRIMARY KEY ( tag_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sc_site_textedit;

CREATE TABLE sc_site_textedit
(
	ste_id      INT UNSIGNED NOT NULL AUTO_INCREMENT,
	ste_type    TINYINT UNSIGNED DEFAULT NULL,
	ste_title   VARCHAR(100) NOT NULL,
	ste_content TEXT,
	PRIMARY KEY ( ste_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sc_site_texts;

CREATE TABLE sc_site_texts
(
	txt_id        INT UNSIGNED NOT NULL AUTO_INCREMENT,
	txt_id_parent INT UNSIGNED NOT NULL DEFAULT '0',
	txt_app_name  VARCHAR(10)  NOT NULL,
	txt_app_mode  VARCHAR(10)  NOT NULL,
	txt_app_id    INT UNSIGNED NOT NULL,
	txt_lang      VARCHAR(2)   NOT NULL,
	txt_type      VARCHAR(10)  NOT NULL,
	txt_dt_add    TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
	txt_dt_mod    TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	txt_content   TEXT,
	PRIMARY KEY ( txt_id )
)
	ENGINE = InnoDB;

DROP TABLE IF EXISTS sc_smartlinks;

CREATE TABLE sc_smartlinks
(
	sl_id       INT UNSIGNED NOT NULL AUTO_INCREMENT,
	sl_dt_add   TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
	sl_dt_mod   TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	sl_dt_end   DATETIME              DEFAULT NULL,
	sl_app      VARCHAR(10)  NOT NULL,
	sl_act_name VARCHAR(10)  NOT NULL,
	sl_act_id   INT UNSIGNED NOT NULL,
	sl_key      VARCHAR(128) NOT NULL,
	sl_status   TINYINT UNSIGNED      DEFAULT '1',
	PRIMARY KEY ( sl_id )
)
	ENGINE = InnoDB;
