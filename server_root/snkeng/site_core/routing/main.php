<?php
/*
 * You get here after checking all restricted first path segments (api, server, user, res, partial,...)
 * First definition of actual site behaviour, should link to the apps or direct pages from here
 */

//
switch ( \snkeng\core\engine\nav::current() ) {
	//
	case 'dp':
		// Remove DP
		\snkeng\core\engine\nav::next();

		// Convert path to filename
		$file_name = str_replace('/', '_', \snkeng\core\engine\nav::remaining());
		$file_path = $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/pages/direct/' . $file_name . '.php';

		//
		if ( !file_exists($file_path) ) {
			\snkeng\core\engine\nav::invalidPage();
		}

		//
		require $file_path;
		break;

	//
	case '':
	case null:
		require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_core/pages/simple/#index.php';
		break;

	// Otros
	default:
		seLoad_appParams('site', true);
		require $_SERVER['DOCUMENT_ROOT'] . '/snkeng/site_modules/site/routing/usr/pages/#map.php';
		break;
}
//
