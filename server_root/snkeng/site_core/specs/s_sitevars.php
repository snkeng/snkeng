<?php
//
return [
	'server' => [
		'test' => false,
		'local' => false,
		'base_path' => '',
		'url' => ''
	],
	'site' => [
		'url' => 'http://www.example.com',
		'surl' => 'https://www.example.com',
		'img' =>        '/snkeng/site_core/res/img/logos/normal.jpg',
		'img_square' => '/snkeng/site_core/res/img/logos/square.jpg',
		'img_banner' => '/snkeng/site_core/res/img/logos/banner.jpg',
		'name' => 'Example',
		'desc' => 'A description for missing information for sharing.',
		'domain' => 'example.com',
		'baseurl' => 'www.example.com',
		//
		'langs' => ['en_US'],
		'lang' => 'en_US'
	],
	'page' => [
		'complete' => false,
		'template' => 'main',
		'base' => '' // Empty -> defaults to system
	]
];