<?php
// Configuración
return [
	'locations' => [
		'example.testserver.com' => [
			'lang' => 'en_US',
			'local' => true,
		],
		'www.example.com' => [
			'lang' => 'en_US',
			'local' => false,
		],
		'example.com' => [
			'redirect' => 'https://www.example.com'
		],
	]
];
