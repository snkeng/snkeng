<?php
//
return [
	'none' => [
		'name' => 'No asignado',
		'apps' => []
	],
	'blogger' => [
		'name' => 'Blogger',
		'apps' => [
			'sitez' => [
				'blog', 'blog_add', 'blog_upd',
				'images', 'images_add','images_upd',
			]
		]
	],
	'siteadm' => [
		'name' => 'Site administrator',
		'apps' => [
			'sitez' => [
				'all'
			]
		]
	],
];
