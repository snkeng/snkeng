<?php
//
$year = date("Y");
$siteLogo = "<img src='{$siteVars['site']['img']}' alt='{$siteVars['site']['name']}' />";




//
return [
	//
	'template_style' => <<<CSS
/* Core */
html { font-size:62.5%; }
body { margin:0; background-color:#F5F5F5; font-size:1.6rem; font-family:Arial, Helvetica, sans-serif; }
main { margin:60px 10px 10px; border:1px solid #DDD; padding:10px; background-color:#FFF; }
footer { margin:20px; display:flex; justify-content:center; align-items:center; }
/* */
amp-img { background-color: gray; }
/* icon support */
.icon { vertical-align:middle; }
svg.icon { display:inline-block; height:1em; width:1em; vertical-align:baseline; fill:currentColor; }
svg.icon.inline { display:inline-block; }
svg.icon.inline.mr { margin-right:0.5em; }
svg.icon.inline.ml { margin-left:0.5em; }
/* Menu */
:root {
	--color-main-00-norm-n:#FFF;
	--color-main-00-comp-n:#333;

	--color-main-01-norm-n:#43525F;
	--color-main-01-comp-n:#FFF;
	--color-main-01-norm-h:#43525F;
	--color-main-01-comp-h:#FFF;

	--color-main-02-norm-n:#657E92;
	--color-main-02-comp-n:#FFF;
	--color-main-02-norm-h:#657E92;
	--color-main-02-comp-h:#FFF;

	--color-main-03-norm-n:#7295B2;
	--color-main-03-comp-n:#FFF;
	--color-main-03-norm-h:#5C7994;
	--color-main-03-comp-h:#DDD;

	--color-main-07-norm-n:#CCC;
	--color-main-07-comp-n:#333;
	--color-main-07-norm-h:#BBB;
	--color-main-07-comp-h:#111;
}

/* Navigation */
.mobile_nav { height:50px; display:flex; position:fixed; top:0; left:0; width:100vw; flex-direction:column; z-index:1000; background-color:orange; }
.mobile_nav.full { height:100vh; display:flex; }
.mobile_nav .bar { height:50px; background-color:var(--color-main-01-norm-n); color:var(--color-main-01-comp-n); display:flex; justify-content:space-between; }
.mobile_nav .bar > * { height:100%; }
.mobile_nav .bar .logo { display:inline-flex; justify-content:center; align-items:center; }
.mobile_nav .bar .logo img { display:block; height:40px; }
.mobile_nav .bar button { cursor:pointer; background:none; border:0; color:#FFF; font-size:1.6rem; }
.mobile_logo { height:40px; width:50px; }
.mobile_logo .snk_text, .mobile_logo .snk_body { fill:#FFF; }
.mobile_logo .snk_tounge { fill:#F00; }
/* */
.mobile_nav_menu {}
.mobile_nav_menu button { position:absolute; display:inline-flex; justify-content:center; align-items:center; width:50px; height:50px; top:0; left:0; }
.mobile_nav_menu button:hover { background-color:#262626}
.mobile_nav_menu input[type="checkbox"] { position:absolute; top:0; left:0; width:50px; height:50px; opacity:0; margin:0; cursor:pointer; z-index:1; }
.mobile_nav_menu .navigation { position:absolute; top:50px; left:-80vw; width:80vw; z-index:5; height:calc(100vh - 50px); background-color:var(--color-main-02-norm-n); color:var(--color-main-02-comp-n); transition:all 0.3s ease-in-out; }
.mobile_nav_menu .background { position:absolute; top:0; left:0; z-index:-1; width:100vw; height:100vh; display:none; background-color:#333333AA; }

.mobile_nav_menu input[type="checkbox"]:checked { z-index:2; width:100vw; height:100vh; }
.mobile_nav_menu input[type="checkbox"]:hover ~ button { background-color:#5A5A5A; }
.mobile_nav_menu input[type="checkbox"]:checked ~ button { background-color:#5A5A5A; }
.mobile_nav_menu input[type="checkbox"]:checked ~ .navigation { left:0; }
.mobile_nav_menu input[type="checkbox"]:checked ~ .background { display:block; }
/* */
.mobile_nav_menu .mainMenu a { display:block; padding:15px 20px; color:var(--color-main-02-comp-n); font-size:1.6rem; }
.mobile_nav_menu .mainMenu a + a { border-top:1px solid #ADADAD; }
.mobile_nav_menu .mainMenu a:hover { background-color:#161616; }
.mobile_nav_menu .mainMenu { margin-bottom:10px; }
/**/
.mobile_nav_menu.right button { left:auto; right:0; }
.mobile_nav_menu.right input[type="checkbox"] { left:auto; right:0; }
.mobile_nav_menu.right input[type="checkbox"]:checked { z-index:1; width:100vw; height:100vh; top:0; right:0; }
.mobile_nav_menu.right input[type="checkbox"]:checked ~ .navigation { right:0; left:auto; }
.mobile_nav_menu.right .navigation { right:-80vw; left:auto; }

CSS
	,
	//
	'template_body' => <<<HTML
<!-- INI:Header -->
<header class="mobile_nav">
	<div class="bar">
		<div class="mobile_nav_menu" se-elem="mobileMenu">
			<input type="checkbox" />
			<button>
				<svg class="icon"><use xlink:href="#fa-navicon" /></svg>
			</button>
			<div class="background"></div>
			<div class="navigation" role="navigation" aria-label="Main navigation">
				<div class="mainMenu">
					<nav>
						<a se-nav="se_middle" se-mode="mobile" class="main" href="/">
							<svg class="icon inline mr"><use xlink:href="#fa-home" /></svg>
							Inicio
						</a>
						<a se-nav="se_middle" se-mode="mobile" class="main" href="/blog/">Blog</a>
						<a se-nav="se_middle" se-mode="mobile" class="main" href="/contacto/">Contact</a>
					</nav>
				</div>
			</div>
		</div>
		<div class="logo">{$siteLogo}</div>
		<div class="mobile_nav_menu right" se-elem="mobileMenu">
		</div>
	</div>
</header>
<!-- END:Header -->

<!-- INI:Body -->
<main>
!content_body;
</main>
<!-- END:Body -->

<!-- INI:Footer -->
<footer>
<div>{$siteVars['site']['name']} {$year}</div>
</footer>
<!-- END:Footer -->\n
HTML
	,
	//
	'svg_icons' => [
		['fa-navicon', '0,0,1536,1792', '<path d="M1536,1344l0,128q0,26-19,45t-45,19l-1408,0q-26,0-45-19t-19-45l0-128q0-26,19-45t45-19l1408,0q26,0,45,19t19,45z M1536,832l0,128q0,26-19,45t-45,19l-1408,0q-26,0-45-19t-19-45l0-128q0-26,19-45t45-19l1408,0q26,0,45,19t19,45z M1536,320l0,128q0,26-19,45t-45,19l-1408,0q-26,0-45-19t-19-45l0-128q0-26,19-45t45-19l1408,0q26,0,45,19t19,45z"/>'],
		['fa-home', '0,0,1664,1792', '<path d="M1408,992l0,480q0,26-19,45t-45,19l-384,0l0-384l-256,0l0,384l-384,0q-26,0-45-19t-19-45l0-480q0-1,0.5-3t0.5-3l575-474l575,474q1,2,1,6z M1631,923l-62,74q-8,9-21,11l-3,0q-13,0-21-7l-692-577l-692,577q-12,8-24,7q-13-2-21-11l-62-74q-8-10-7-23.5t11-21.5l719-599q32-26,76-26t76,26l244,204l0-195q0-14,9-23t23-9l192,0q14,0,23,9t9,23l0,408l219,182q10,8,11,21.5t-7,23.5z"/>'],
	]
	//
];
//
