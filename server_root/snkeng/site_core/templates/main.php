<?php
//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);

// Archivos a cargar
if ( \snkeng\core\engine\login::check_loginLevel('admin') ) {
	\snkeng\core\engine\nav::svgIconsAdd('font-awesome-4-7-0');
} else {
	\snkeng\core\engine\nav::svgIconsAdd('font-awesome-4-7-0');
}

// General files
\snkeng\core\engine\nav::pageFileGroupAdd(['site_core_template_main']);
// \snkeng\core\engine\nav::pageFileGroupAdd(['site_template_main', 'core_main']);
\snkeng\core\engine\nav::pageFileModuleAdd('site_core', '', '/site.mjs');
\snkeng\core\engine\nav::pageFileModuleAdd('site_core', '', '/template/main.mjs');

$curYear = date("Y");

$page['body'] = <<<HTML
<div id="se_main">
<!-- INI:Header -->
<header>

<div class="site_nav_mobile">
	<div class="bar">
		<site-mobile-nav data-state="closed">
			<button data-dom-element="button" aria-controls="navigation-mobile-main"><svg class="icon inline"><use xlink:href="#fa-navicon" /></svg></button>
			<div data-dom-element="background" class="background"></div>
			<nav data-dom-element="navigation" id="navigation-mobile-main" class="navigation" aria-label="Main navigation" aria-expanded="false">
				<div class="mainMenu">
					<a se-nav="se_middle" class="main" href="/"><svg class="icon inline"><use xlink:href="#fa-home" /></svg></a>
					<a se-nav="se_middle" class="main" href="/docs/">Documentation</a>
					<a se-nav="se_middle" class="main" href="/blog/">Blog</a>
					<a se-nav="se_middle" class="main" href="/contact/">Contact</a>
				</div>
			</nav>
		</site-mobile-nav>
		<div class="logo_placeholder">
			<svg class="logo_icon"><use xlink:href="#logo" /></svg>
			<span class="logo_text">SNKENG Starting Template</span>
		</div>
		<div></div>
	</div>
</div>

<div class="site_nav_desktop template_grid_borders">
	<div class="gr_content main_menu">
		<a se-nav="se_middle" class="logo_link" href="/">
			<svg class="logo_icon"><use xlink:href="#logo" /></svg>
			<span class="logo_text">SNKENG Starting Template</span>
		</a>
		<div>
			<nav>
				<a se-nav="se_middle" class="main" href="/"><svg class="icon inline"><use xlink:href="#fa-home" /></svg></a>
				<a se-nav="se_middle" class="main" href="/docs/">Documentation</a>
				<a se-nav="se_middle" class="main" href="/blog/">Blog</a>
				<a se-nav="se_middle" class="main" href="/contact/">Contact</a>
			</nav>
		</div>
	</div>
</div>

</header>
<!-- END:Header -->\n
<!-- INI:MAIN -->
<main id="se_middle">
{$page['body']}
</main>
<!-- END:MAIN -->\n
<!-- INI:Foot -->
<footer class="template_grid_borders">
	<div class="gr_content">
		<div>
			<div class="footer-links">
				<a class="btn_snFA" target="_blank" href="/blog/feed/rss2/" title="RSS Feed">
					<svg class="icon"><use xlink:href="#fa-feed" /></svg>
				</a>
			</div>
		</div>
		<div>SNKENG Starting Template 2012-{$curYear}</div>
	</div>
</footer>
<!-- END:Foot -->
</div>
<svg version="1.1" style="display:none;">
<symbol id="logo" viewBox="0,0,600,600">
	<g transform="translate(0 -452)">
		<path class="snk_tounge" d="m248 1.03e3c-9.41-14.4-10.5-14.7-17.7-4.84-13.1 17.9-15 4.96-2.66-18.1 11.1-20.7 11.2-23 2.12-42-6.77-14.2-17-19.7-17-19.7 4.52-1.65 11.9-3.85 24.4-7.77 0 0-5.49 7.2 1.78 22.2 4.1 8.45 7.08 21.9 7.08 29.7 0 19.5 12 44.1 21.5 44.1 4.33 0 7.88 2.65 7.88 5.88 0 11.6-17.6 5.57-27.4-9.45z" />
		<path class="snk_body" d="m211 953c-9.03 1.44-31.4 3.31-63.1 4.34-31.8 1.04-39.9 0.406-73.6-14.9-17.1-10-41.3-31-50.1-45-14.6-25-23.1-28-22.9-78 0.18-43 6.93-60 16.9-81 18.2-38.4 67.1-98.3 99.9-116 35.8-19 110-25.5 173-22.4 37 1.87 45.5 4.5 65 20.2 32.5 26.2 59.2 81.6 67.2 129 11.5 68 22.2 92.6 51.6 118 28.9 25.4 31.7 34.4 3.67 14.4-31.1-22.1-48.9-51.5-60.5-109-17.6-87.9-49.2-145-95.4-160-14.8-4.9-46.7-5.35-97.5-2.14-81.8 5.18-115 14.6-164 79.4-38.6 50.9-49.1 80.4-49.1 137 0 47.6 30.3 86.2 78.1 110 16.3 8.25 38.5 11.3 70.4 10.2 76.2-2.6 87.3-18 115-45.3 22.6-22.6 54.5-81.5 54.5-101 0.003-5.66 2.88-10.3 6.4-10.3 4.18 0 5.05 7.95 2.53 22.9-2.42 14.3-0.174 26.2 2.56 30 5.61 7.9 12.3 17.2 22.6 35.5 30.2 53.6 77.5 93.4 120 93.8 8.89 0.0706 16.2 2.78 16.2 6.01 0 16.9-62.6-3.8-97.1-32.1-13.6-11.1-35.9-39.1-49.6-62.3l-25-42.1-16.6 29c-15.6 27.4-59.6 65.9-64.3 68.9-4.63 2.98-26.2 10.7-35.2 12.1zm-125-207c-7.16-7.16-4.05-19.6 4.9-19.6 4.9 0 8.82 5.23 8.82 11.8 0 11.7-6.27 15.3-13.7 7.84zm130-12c-0.383-2.43-0.415-6.3-0.0706-8.61s-3.67-2.55-8.93-0.533c-6.4 2.46-8.68 1.05-6.91-4.25 3.69-11.1 49.4-25.3 67.1-20.8 8.08 2.03 13.1 6.3 11.1 9.49-1.97 3.19-6.01 4.31-8.96 2.48-2.96-1.83-15-1.51-26.8 0.705-17.2 3.22-19.5 5.11-11.7 9.47 5.35 2.99 8.19 7.93 6.31 11-4.06 6.57-20.1 7.41-21.1 1.11zm-164-6c0-8.78 26.8-25.1 41.2-25.1 6.74 0 20.7 4.5 31.1 10 15.3 8.14 16.8 10.4 7.9 12.3-6 1.27-13.8-0.577-17.3-4.11-9.66-9.66-39.9-7.74-52.1 3.3-5.91 5.35-10.8 6.98-10.8 3.62zm129-30.4c0-2.89 3.55-6.61 7.89-8.28s12.4-12.5 17.9-24.2c7.39-15.6 13.7-21.1 24.1-21.1 7.74 0 15.7 2.65 17.7 5.88 2.05 3.32-2.75 5.88-11 5.88-9.02 0-16 4.13-18.1 10.7-6.76 21.3-38.6 46.9-38.6 31.1z"/>
		<g class="snk_text" transform="matrix(5.88 0 0 5.88 -.398 -5.14e3)">
			<path d="m59.8 970c1.53-2.96 1.5-3.09-1.47-5.59-2.29-1.92-3.04-3.4-3.04-5.95 0-4.87 3.25-6.71 12-6.79 5.96-0.0546 7.44 0.181 9.62 2.23 3.14 2.95 1.93 6.93 0.0836 10.2-1.63 2.9-3.03 3.46-6.25 3.72-2.24 0.183-5.35 1.38-7.15 2.75-4.37 3.33-5.73 3.12-3.8-0.604zm10.6-3.6c2.33-0.347 4.15-0.575 5.53-3.25 3-5.84 2.56-10.2-8.5-10.1-8.87 0.134-11.7 3.27-10.2 7.14 0.57 1.5 2.12 2.65 3.33 3.89 1.11 1.14 2.3 1.97 1.25 4.82l-0.688 1.86 3.39-2.72c1.62-1.3 3.21-1.27 5.91-1.68zm0.344-7.09c0-0.582 0.28-0.951 0.83-0.611s0.585 0.689 0.447 0.889c-0.255 0.37-0.152 0.441-0.702 0.441s-0.575-0.136-0.575-0.719z"/>
			<path d="m67.6 958c-0.313-0.0294-0.799 0.241-0.792 0.586 0.0053 0.267 0.295 0.517 0.538 0.542 0.257 0.0265 0.723-0.126 0.775-0.403 0.0616-0.33-0.214-0.696-0.521-0.725z"/>
			<path d="m63.2 958c-0.326 0.0822-0.428 0.558-0.311 0.88 0.114 0.314 0.422 0.439 0.722 0.306 0.24-0.107 0.412-0.47 0.336-0.727-0.0842-0.285-0.466-0.53-0.748-0.459z"/>
		</g>
	</g>
</symbol>
</svg>\n
HTML;
//


//
$page['head']['extraHead'].= <<<HTML
<link rel="alternate" href="/blog/feed/rss2/" title="{$siteVars['site']['name']} RSS2" type="application/rss+xml">
<link rel="alternate" href="/blog/feed/atom/" title="{$siteVars['site']['name']} ATOM" type="application/atom+xml">
<meta name="twitter:site:id" content="">
<meta name="twitter:site:name" content="">\n
HTML;
//
