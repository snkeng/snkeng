<?php



return <<<HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>!title;</title>
<style>
body { width:100%!important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; }
.ExternalClass { width: 100%; }
span{ font-family:'Segoe UI', Arial, sans-serif; font-size:15px; line-height:20px; color:#555; }
@media only screen and (max-width: 600px)
{
table[class="content_wrap"] { width: 94%!important; }
table[class="full_width"] { width: 100%!important; }
table[class="hide"], img[class="hide"], td[class="hide"] { display: none !important; }
td[class="text-center"] { text-align: center!important; }
a[class="button"] { border-radius:2px; background-color:#308F9B; color:#FFF!important; padding: 5px; display:block; text-decoration: none; text-transform: uppercase; margin: 0 0 10px 0;}
}
</style>
</head>
<body bgcolor="#FFF" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="-webkit-font-smoothing:antialiased;width:100% !important;background-color:#EFEFEF;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;-webkit-text-size-adjust:none;">
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#900" style="font-family:'Segoe UI', Arial, sans-serif;font-size:14px;line-height:20px;color:#555;" role="presentation">
<tr><td width="100%" bgcolor="#EFEFEF">
<table width="500" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#FFF">
<tr><td width="100%" height="10"></td></tr>
<!--INI:Email-->
<tr><td>
<!--INI:Banner-->
<table width="100%">
<tr>
<td width="5%"></td>
<td width="90%">
<a href="{$siteVars['site']['surl']}" style="color:#FFF; font-weight:bold;" target="_blank" style="display:block;"><img src="{$siteVars['site']['surl']}/{$siteVars['site']['img']}" alt="{$siteVars['site']['name']}" /></a>
</td>
<td width="5%"></td>
</tr>
</table>
<!--END:Banner-->
</td></tr>
<tr><td width="100%" height="20"></td></tr>
<!-- Ini:Section -->
<tr><td>
	<table width="100%">
	<tr>
	<td width="5%"></td>
	<td width="90%">
	!message;
	</td>
	<td width="5%"></td>
	</tr>
	</table>
</td></tr>
<!-- END:Section -->
<tr><td width="100%" height="20"></td></tr>
<!-- Ini:Section -->
<tr><td>
	<table width="100%">
	<tr>
	<td width="5%"></td>
	<td width="90%">
	<div style="font-size:0.8em; color:#777;"><p>This message was automatically genearted. Please do not respond.</p></div>
	</td>
	<td width="5%"></td>
	</tr>
	</table>
</td></tr>
<!-- END:Section -->
<tr><td width="100%" height="20"></td></tr>
<!-- Ini:Section But -->
<tr><td width="100%" height="20" bgcolor="#FF8000"></td></tr>
<tr><td width="100%" bgcolor="#FF8000" align="center"><a href="{$siteVars['site']['surl']}" style="color:#FFF; font-weight:bold;" target="_blank">{$siteVars['site']['surl']}</a></td></tr>
<tr><td width="100%" height="20" bgcolor="#FF8000"></td></tr>
<!-- END:Section But -->
<!--END:Email-->
</table>
</td></tr>
</table>
</body>
</html>
HTML;
//
