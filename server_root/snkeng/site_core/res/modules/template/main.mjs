// Site mobile navigation bar element
customElements.define("site-mobile-nav", class extends HTMLElement {
	menuToggle = this.querySelector('[data-dom-element="button"]');
	navBackground = this.querySelector('[data-dom-element="background"]');
	siteNavigation = this.querySelector('[data-dom-element="navigation"]');

	inTransition = false;

	constructor() {
		// Always call super first in constructor
		super();
	}

	connectedCallback() {
		//
		this.dataset['state'] = 'closed';
		// Callbacks
		this.menuToggle.addEventListener('click', this.toggleMenu.bind(this));
		this.navBackground.addEventListener('click', this.closeMenu.bind(this));

		// Listen for page change to close
		document.addEventListener('pageLoadEvent', (e) => {
			this.closeMenu();
		}, false);
	}

	//
	toggleMenu() {
		// avoid double tap?
		if ( this.inTransition ) { return; }

		const isOpened = this.siteNavigation.getAttribute('aria-expanded') === "true";
		if ( isOpened ? this.closeMenu() : this.openMenu() ) ;
	}

	openMenu() {
		this.siteNavigation.setAttribute('aria-expanded', "true");
		this.dataset['state'] = 'open';
		this.inTransition = true;

		// Set callback to hide elements
		this.siteNavigation.addEventListener('transitionend', () => {
			this.inTransition = false;
		}, {once: true});
	}

	closeMenu() {
		// Ignore if closed (as if sent by a common navigation link and cached through event listener)
		if ( this.dataset['state'] === 'closed' || this.dataset['state'] === 'close' ) {
			return;
		}

		// Set object attributes
		this.siteNavigation.setAttribute('aria-expanded', "false");
		this.dataset['state'] = 'close';
		this.inTransition = true;

		// Set callback to hide elements
		this.siteNavigation.addEventListener('transitionend', () => {
			this.inTransition = false;
		}, {once: true});
	}
});
