import * as core from '/snkeng/core/res/modules/library/site-core.mjs';
import * as domManipulator from '/snkeng/core/res/modules/library/dom-manipulation.mjs';

// Define service worker
core.serviceWorkerSet('/sw.js')

// Navigation setup
core.basicSetup();

// Page load events, Google Tag / Analytics set
document.addEventListener('pageLoadEvent', (e) => {
	// Google v3
	if ( typeof _gaq !== 'undefined' ) {
		_gaq.push(['_trackPageview', e.detail.cUrl]);
	}
	// Google tag
	if ( typeof dataLayer !== 'undefined' ) {
		gtag('send', 'pageview');
	}
}, false);