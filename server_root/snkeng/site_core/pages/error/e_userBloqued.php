<?php
$page['header']['title'] = 'Blocked account';
$page['body'] = <<<HTML
<div class="fullTitle">
	<div class="wpContent"><h1>Blocked account</h1></div>
</div>
<div class="section">
<div class="wpContent">
El acceso al sitio se encuentra bloqueado para esta cuenta.<br />
<a se-nav="se_middle" href="/?logout=true">Cerrar Sesión.</a>
</div>
</div>
HTML;
