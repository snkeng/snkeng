<?php
// Cache final check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$page['head']['metaDesc'] = "SNK Framework - Starting Template";

//
// Página
$page['body'] = <<<HTML
<style>
.page-index {
	font-size:1.8em;
	
	& .hero {
		min-height:60vh;
		max-height:95vh;
		position:relative;
		
		background-color:#000;
		
		& img {
			width:100%;
			max-width: 1200px;
			position: absolute;
			display:block;
			aspect-ratio: 2 / 1;
			max-height: 100%;
			top:0;
		}
		
		& .text {
			color:#FFF;
			text-shadow: #000 1px 1px;
			padding: 0.5em;
			background-color: #00000033;
			border-radius: 0.2em;
			position: absolute;
			bottom: 50px;
			right: 0;
		}
	}
	
	& .section {
		padding:2em;
		max-width: 80%;
		margin: 0 auto;
		& .title {
			font-size:1.6em;
			text-align: center;
			font-weight: bold;
			margin-bottom: 0.5em;
		}
	}
	
	
	@media (width < 700px) {
		& img { min-height: 100vh; object-fit:cover; grid-column: full-width; }
		& .textContainer { width:100%; height: 100vh; text-align: center; top:0; bottom:auto; padding:1em;  }
		& .text-big { font-size: 3rem; max-width:unset; }
		& .text-small { font-size: 2.3rem; max-width:unset; }
	}
}
</style>
<div class="page-index template_grid_borders">
	<div class="gr-fullwidth gr-subgrid hero">
		<div class="gr-breakout">
			<picture>
				<source type="image/avif" srcset="/res/image/site_content/site_core/pages/david-clode-5uU8HSpfwkI-unsplash.w_1200.avif" />
				<source type="image/webp" srcset="/res/image/site_content/site_core/pages/david-clode-5uU8HSpfwkI-unsplash.w_1200.webp" />
				<img class="image" src="/res/image/site_content/site_core/pages/david-clode-5uU8HSpfwkI-unsplash.w_1200.jpg" width="1200" heigth="600" alt="alt" decoding="async" />
			</picture>
			<div class="text gr-content">Starting Template</div>
		</div>
	</div>
	<div class="section">
		<div class="title">SNKENG Template</div>
		<div class="text">
			<p>This is an example of the main template.</p>
			<p>The index is a single page file called directly from the routing (as in no dynamic loading).</p>
		</div>
	</div>
</div>
HTML;
//
