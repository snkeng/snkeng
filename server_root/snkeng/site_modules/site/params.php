<?php
// 
return [
	'name' => 'site',
	'langs' => ['en_US', 'es_MX'],
	'dir' => __DIR__,
	'permissions' => [],
	'settings' => [
		'blog' => [
			'name' => 'blog',
			'postUrl' => '/blog/!urlTitle;-!idPad;/',
			'bloggerFocus' => 0,
			'comments' => 3,
			'content' => 'md'
		],
	]
];