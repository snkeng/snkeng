<?php

// Cache final check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

// SITIO - Contact
$page['head']['title'] = 'Contact';
$page['head']['metaWord'] = 'Contact';
$page['head']['metaDesc'].= 'Send information request.';
//
$page['head']['url'] = '/contacto/';

//
$referer = ( isset($_SERVER['HTTP_REFERER']) ) ? $_SERVER['HTTP_REFERER'] : '';

//
$textData = \snkeng\core\site\simpleText::load(['site_contact']);

//
\snkeng\core\engine\nav::pageFileModuleAdd('core', '', '/components-simple/se-async-form.mjs');
\snkeng\core\engine\nav::pageFileModuleAdd('core', '', '/components-simple/se-async-form.css');
\snkeng\core\engine\nav::pageFileModuleAdd('core', '', '/components-simple/se-textarea-autoadjust.mjs');

//<editor-fold desc="Re Captcha">
//
// $page['files']['ext'] = ['https://www.google.com/recaptcha/api.js?render=explicit&hl=es-419&onload=captchaLoad'];
$captchaKey = '';
if ( $_ENV['GOOGLE_CAPTCHA_V2_KEY'] ) {
	// Add file to download
	$page['files']['ext'] = ["https://www.google.com/recaptcha/api.js"];
	$captchaKey = $_ENV['GOOGLE_CAPTCHA_V2_KEY'];
}
//</editor-fold>



// Contact
$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">Contact</div></div>

<style>
#page_contact .textDescription { font-size:1.2em; padding:1em 0; }
#page_contact .se_form { background-color:var(--color-main-04-norm-n); color:var(--color-main-04-comp-n); padding:1.5em; border-radius:0.2em; }
</style>

<div id="page_contact" class="wpContent grid">
	<div class="gr_sz04 gr_ps02">\n
		<form class="se_form" method="post" action="/api/module_usr/site/contact/mail" is="se-async-form">
			<input type="hidden" name="ref" value="{$referer}" />
			<input type="hidden" name="lName" value="{$referer}" />
			<label class="separator required">
				<div class="cont"><span class="title">Name</span><span class="desc"></span></div>
				<input type="text" name="uName" required pattern="[A-z\u00C0-\u00FF ]+" minlength="3" maxlength="40" title="Name">
			</label>
			<label class="separator required">
				<div class="cont"><span class="title">E-Mail</span><span class="desc"></span></div>
				<input type="email" name="eMail" required title="Requerido para establecer contacto.">
			</label>
			<label class="separator required">
				<div class="cont"><span class="title">Message</span><span class="desc"></span></div>
				<textarea name="uMsg" rows="4" maxlength="500" required is="se-textarea-autoadjust"></textarea>
			</label>
			<div class="g-recaptcha" data-sitekey="{$captchaKey}"></div>
			<button type="submit"><svg class="icon inline mr"><use xlink:href="#fa-mail-forward" /></svg>Enviar</button>
			<output data-elem="response"></output>
		</form>
	</div>
	<div class="gr_sz04">
		{$textData['elems']['site_contact']}
	</div>
</div>
HTML;
//