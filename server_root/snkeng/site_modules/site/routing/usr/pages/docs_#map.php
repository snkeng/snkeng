<?php
// Load defaults

// Default user documentation modifier
\snkeng\core\engine\nav::pageFileModuleAdd('site_modules', 'site', '/pages/documentation/navigation.css');

// Remover docs
\snkeng\core\engine\nav::next();

//
switch ( count(\snkeng\core\engine\nav::$pathElementList) ) {
	//
	case 0:
		se_killWithError('but how?');
		break;

	//
	case 1:
		// Mostrar documentación disponible
		require __DIR__ . '/docs_#index.php';
		break;

	//
	case 2:
		$docTitle = preg_replace('/[^A-Za-z0-9\/\-_]/', '', \snkeng\core\engine\nav::next());

		//
		$sql_qry = <<<SQL
SELECT
docs.docs_id AS id,
docs_published AS isPub, docs_access_level AS accessLevel, docs_access_link_req AS isLinkReq,
docs_content_bundles AS contentBundles, docs_content_body_mod AS contentBodyMod
FROM sc_site_documents AS docs
WHERE docs.docs_title_url='{$docTitle}'
LIMIT 1;
SQL;
		$params['docData'] = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [
			'int' => ['id', 'accessLevel'],
			'bool' => ['isPub', 'isLinkReq']
		]);

		//
		if ( empty($params['docData']) ) { \snkeng\core\engine\nav::invalidPage(); }

		// Mostrar índice documentación específica
		require __DIR__ . '/docs_[id]_main.php';
		break;

	//
	default:
		$cUrl = preg_replace('/[^A-Za-z0-9\/\-_]/', '', \snkeng\core\engine\nav::$pathStr);

		//
		$sql_qry = <<<SQL
SELECT
art_id AS id, cat_id AS catId,
art_title AS title, art_urltitle AS urlTitle, art_url AS fullUrl,
art_dt_mod AS dtMod,
art_order_hierarchy AS hierarchy,
art_meta_word AS metaWord, art_meta_desc AS metaDesc,
art_order_level AS orderLvl,
art_content AS content, art_content_extra AS contentExtra
FROM sc_site_articles
WHERE art_type_primary='docs' AND art_url='{$cUrl}'
LIMIT 1;
SQL;
		$params['page'] = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [
			'int' => ['id', 'catId', 'orderLvl']
		]);

		//
		if ( empty($params['page']) ) { \snkeng\core\engine\nav::invalidPage(); }

		//
		$sql_qry = <<<SQL
SELECT
docs.docs_id AS id,
docs.docs_title_normal AS title, docs.docs_title_url AS urlTitle,
docs_published AS isPub, docs_access_level AS accessLevel, docs_access_link_req AS isLinkReq,
docs_content_bundles AS contentBundles, docs_content_body_mod AS contentBodyMod
FROM sc_site_documents AS docs
WHERE docs.docs_id='{$params['page']['catId']}'
LIMIT 1;
SQL;
		$params['docData'] = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [
			'int' => ['id', 'accessLevel'],
			'bool' => ['isPub', 'isLinkReq']
		]);

		//
		if ( empty($params['docData']) ) { \snkeng\core\engine\nav::invalidPage(); }

		// Analisis de permisos
		require __DIR__ . '/../../../func/permits.php';
		$params['docPermits'] = docs_get_permits_kill($params['docData']['id'], $params['docData']['isPub'], $params['docData']['accessLevel'], $params['docData']['isLinkReq']);

		// Update last page if is link req
		if ( $params['docData']['isLinkReq'] ) {
			$userId = \snkeng\core\engine\login::getUserId();
			$upd_qry = "UPDATE sc_site_documents_access SET dacc_last_page={$params['page']['id']} WHERE dacc_id={$params['docPermits']['id']}";
			\snkeng\core\engine\mysql::submitQuery($upd_qry);
		}

		// Cache checking
		\snkeng\core\engine\nav::cacheCheckDate($params['page']['dtMod']);
		\snkeng\core\engine\nav::cacheFinalCheck();

		//
		if ( empty($params['page']) ) { \snkeng\core\engine\nav::invalidPage(); }

		//
		if ( isset($_GET['amp']) ) {
			require __DIR__ . '/docs_[id]_page_amp.php';
			exit();
		} else {
			require __DIR__ . '/docs_[id]_page_html.php';
		}
		break;
}
//
