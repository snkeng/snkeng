<?php
// BLOG - Post - Amp
// debugVariable($conditions['app']['blog']);

//
// Datos post
$sql_qry = <<<SQL
SELECT
	art.art_id AS pid, art.art_title AS title, art.art_urltitle AS purltitle, art.art_url AS fullUrl,
	art.art_meta_desc AS metaDesc, art.art_meta_word AS metaWord, art.art_content AS content,
	art.art_type_secondary AS typeSub, art.art_content_extra AS contentExtra, art.art_content_extra_b AS contentExtraB,
	DATE_FORMAT(art.art_dt_pub, '%e %M %Y %T') AS dtpub_simple, DATE_FORMAT(art.art_dt_pub, '%Y-%m-%dT%TZ') AS dtpub_iso,
	DATE_FORMAT(art.art_dt_mod, '%e %M %Y %T') AS dtmod_simple, DATE_FORMAT(art.art_dt_mod, '%Y-%m-%dT%TZ') AS dtmod_iso,
	art.com_id AS comId, art.art_img_struct AS image,
	t2.cat_title AS cname, t2.cat_urltitle AS curlname,
	t3.a_id AS userid, t3.a_obj_name AS username, t3.a_url_name_full AS userurl
FROM sc_site_articles AS art
LEFT OUTER JOIN sc_site_category AS t2 ON t2.cat_id=art.cat_id
LEFT OUTER JOIN sb_objects_obj AS t3 ON t3.a_id=art.a_id
WHERE art_id={$params['page']['id']}
LIMIT 1;
SQL;
//
$contents = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [], [
	'errorKey' => '',
	'errorDesc' => ''
]);

// debugVariable($contents);

// URL
$contents['type'] = 'Article';
$contents['fbType'] = 'Article';
$contents['twType'] = 'Article';
$contents['urlCanonical'] = $siteVars['site']['surl'].$contents['fullUrl']."?amp=1";
$contents['image'] = ( !empty($contents['image']) ) ? $contents['image'] : "";


// Imagen
$elHead = '';
switch ( $contents['typeSub'] ) {
	case 'video':
		$contents['elHead'] = $contents['contentExtra'];
		break;
	default:
		$contents['elHead'] = ( !empty($contents['image']) ) ? "<amp-img src='/res/image/img/w_832/{$contents['image']}' layout='responsive' width='400' height='300' sizes='(min-width: 320px) 320px, 100vw'></amp-img>" : '';
		break;
}

// Content
require $_SERVER['DOCUMENT_ROOT'].'/st_engine/priv/library/op_ampPrint.php';

// First clear
$contents['content'] = se_ampContentClear($contents['content']);

//
$imageMetaData = '';
if ( !empty($contents['image']) ) {
	$origImgSize = getimagesize($_SERVER['DOCUMENT_ROOT'].$contents['image']);
	$imageMetaData = <<<TEXT
	"image": {
		"@type": "ImageObject",
		"url": "{$siteVars['site']['surl']}{$contents['image']}",
		"height": {$origImgSize[1]},
		"width": {$origImgSize[0]}
	}
TEXT;
	//
}

//
$contents['metaData'] = <<<TEXT
{
	"@context": "http://schema.org",
	"@type": "NewsArticle",
	"mainEntityOfPage": "http://cdn.ampproject.org/article-metadata.html",
	"headline": "{$contents['title']}",
	"datePublished": "{$contents['dtpub_iso']}",
	"dateModified": "{$contents['dtmod_iso']}",
	"description": "{$contents['metaDesc']}",
	"author": {
		"@type": "Person",
		"name": "{$contents['username']}"
	},
	"publisher": {
		"@type": "Organization",
		"name": "{$siteVars['site']['name']}",
		"logo": {
			"@type": "ImageObject",
			"url": "{$siteVars['site']['surl']}{$siteVars['site']['img']}"
		}
	},
	$imageMetaData
}
TEXT;

$contents['extraClass'] = <<<CSS
.b_time, .b_info { margin-bottom:10px; }

CSS;

$contents['content'] = <<<HTML
<article>
<h1 class="title">{$contents['title']}</h1>
<div class="b_time">
<svg class="icon" viewBox="0 0 24 24"><path class="icon" d="M12,20A8,8 0 0,0 20,12A8,8 0 0,0 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22C6.47,22 2,17.5 2,12A10,10 0 0,1 12,2M12.5,7V12.25L17,14.92L16.25,16.15L11,13V7H12.5Z" /></svg>
<span>{$contents['dtpub_simple']}</span>
</div>
<div class="b_info">
<a class="category" target="_blank" href="{$siteVars['server']['base_path']}/blog/category/{$contents['curlname']}/">
<svg class="icon" viewBox="0 0 24 24"><path d="M10,4H4C2.89,4 2,4.89 2,6V18A2,2 0 0,0 4,20H20A2,2 0 0,0 22,18V8C22,6.89 21.1,6 20,6H12L10,4Z" /></svg>
{$contents['cname']}</a>
<a class="user" target="_blank" href="{$siteVars['server']['base_path']}/blog/autor/{$contents['userurl']}/">
<svg class="icon" viewBox="0 0 24 24"><path d="M12,4A4,4 0 0,1 16,8A4,4 0 0,1 12,12A4,4 0 0,1 8,8A4,4 0 0,1 12,4M12,14C16.42,14 20,15.79 20,18V20H4V18C4,15.79 7.58,14 12,14Z" /></svg> {$contents['username']}</a>
</div>
{$contents['elHead']}
<div class="content">
<!-- INI: ArtBody -->
<div class="adv_text">
{$contents['content']}
</div>
<!-- END: ArtBody -->
</div>
</article>
HTML;

//
se_ampPrint($contents);