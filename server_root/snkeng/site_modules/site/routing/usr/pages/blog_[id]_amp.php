<?php
// BLOG - Post - Amp
// debugVariable($conditions['app']['blog']);

//
// Datos post
$sql_qry = <<<SQL
SELECT
	art.art_id AS pid, art.art_title AS title, art.art_urltitle AS purltitle, art.art_url AS fullUrl,
	art.art_meta_desc AS metaDesc, art.art_meta_word AS metaWord, art.art_content AS content,
	art.art_type_secondary AS typeSub, art.art_content_extra AS contentExtra, art.art_content_extra_b AS contentExtraB,
	DATE_FORMAT(art.art_dt_pub, '%e %M %Y') AS dtpub_simple, DATE_FORMAT(art.art_dt_pub, '%Y-%m-%dT%TZ') AS dtpub_iso,
	DATE_FORMAT(art.art_dt_mod, '%e %M %Y %T') AS dtmod_simple, DATE_FORMAT(art.art_dt_mod, '%Y-%m-%dT%TZ') AS dtmod_iso,
	art.com_id AS comId, art.art_img_struct AS image, art.img_id AS imgId,
	t2.cat_title AS cname, t2.cat_urltitle AS curlname,
	t3.a_id AS userId, t3.a_obj_name AS username, t3.a_url_name_full AS userurl, t3.a_img_crop AS usrImage
FROM sc_site_articles AS art
LEFT OUTER JOIN sc_site_category AS t2 ON t2.cat_id=art.cat_id
LEFT OUTER JOIN sb_objects_obj AS t3 ON t3.a_id=art.a_id
WHERE art_id={$params['page']['pId']}
LIMIT 1;
SQL;
//
$contents = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [], [
	'errorKey' => 'blogPostAmpReadData',
	'errorDesc' => 'No fue posible leer la información del post'
]);

// debugVariable($contents);

// URL
$contents['type'] = 'Article';
$contents['fbType'] = 'Article';
$contents['twType'] = 'Article';
$contents['urlCanonical'] = $siteVars['site']['surl'].$contents['fullUrl'].'?amp=1';
$contents['image'] = ( !empty($contents['image']) ) ? $contents['image'] : "";
$contents['elHead'] = '';


// Imagen
$elHead = '';
switch ( $contents['typeSub'] ) {
	//
	case 'video':
		$contents['elHead'] = $contents['contentExtra'];
		break;
	//
	default:
		if ( !empty($contents['image']) ) {
			// Get image data
			$sql_qry = <<<SQL
SELECT
img.img_height AS height, img.img_width AS width, img.img_desc AS content
FROM sc_site_images AS img
WHERE img_id='{$contents['imgId']}';
SQL;
			$imgData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [], [
				'errorKey' => 'blogPostAmpReadData',
				'errorDesc' => 'No fue posible leer la información del post'
			]);


			//
			$contents['elHead'] = <<<HTML
<div class="headContent">
<amp-img
	src="/res/image/site/w_360/{$contents["image"]}.webp"
	alt="{$imgData["content"]}"
	srcset="/res/image/site/w_360/{$contents["image"]}.webp 360w, /res/image/site/w_720/{$contents["image"]}.webp 720w, /res/image/site/w_1200/{$contents["image"]}.webp 1200w"
	width="{$imgData['width']}" height="{$imgData['height']}" layout="responsive"
>
	<amp-img fallback
		src="/res/image/site/w_360/{$contents["image"]}.jpg"
		alt="{$imgData["content"]}"
		srcset="/res/image/site/w_360/{$contents["image"]}.jpg 360w, /res/image/site/w_720/{$contents["image"]}.jpg 720w, /res/image/site/w_1200/{$contents["image"]}.jpg 1200w"
		width="{$imgData['width']}" height="{$imgData['height']}" layout="responsive"
	>
	</amp-img>
</amp-img>
</div>
HTML;
			//
		}

		break;
}

//
$imageMetaData = '';
if ( !empty($contents['image']) ) {
	$origImgSize = getimagesize($_SERVER['DOCUMENT_ROOT'].'/res/img/site/'.$contents['image'].'.jpg');
	$contents['image'] = "{$siteVars['site']['surl']}/res/img/site/{$contents['image']}.jpg";
	//
	$imageMetaData = <<<TEXT
	"image": {
		"@type": "ImageObject",
		"url": "{$contents['image']}",
		"height": {$origImgSize[1]},
		"width": {$origImgSize[0]}
	}
TEXT;
	//
}

//
$contents['metaData'] = <<<TEXT
{
	"@context": "http://schema.org",
	"@type": "NewsArticle",
	"mainEntityOfPage": "http://cdn.ampproject.org/article-metadata.html",
	"headline": "{$contents['title']}",
	"datePublished": "{$contents['dtpub_iso']}",
	"dateModified": "{$contents['dtmod_iso']}",
	"description": "{$contents['metaDesc']}",
	"author": {
		"@type": "Person",
		"name": "{$contents['username']}"
	},
	"publisher": {
		"@type": "Organization",
		"name": "{$siteVars['site']['name']}",
		"logo": {
			"@type": "ImageObject",
			"url": "{$siteVars['site']['surl']}{$siteVars['site']['img']}"
		}
	},
	$imageMetaData
}
TEXT;
//

// Pre convertion of content
$postContent = '';
$contentMode = 'html';
if ( $conditions['app']['site']['settings']['blog']['content'] === 'md' ) {
	$qry_sel = <<<SQL
SELECT
	txt.txt_id AS id, txt.txt_dt_mod AS dtMod, txt.txt_content AS content
FROM sc_site_texts AS txt
WHERE txt.txt_app_name='site' AND txt.txt_app_mode='blog' AND txt.txt_app_id='{$params['page']['pId']}' AND txt.txt_type='amp'
LIMIT 1;
SQL;
	$textData = \snkeng\core\engine\mysql::singleRowAssoc(
		$qry_sel,
		[
			'int' => ['id', 'parentId'],
			'stripCSlashes' => ['content']
		],
		[
			'errorKey' => '',
			'errorDesc' => '',
		]
	);
	//
	if ( !empty($textData) ) {
		\snkeng\core\engine\nav::cacheCheckDate($textData['dtMod']);
		$postContent = $textData['content'];
		$contentMode = 'amp';
	}
	// Not updated post fallback
	else {
		$postContent = \snkeng\core\site\amp::contentClear($contents['content']);
	}
} else {
	$postContent = \snkeng\core\site\amp::contentClear($contents['content']);
}


\snkeng\core\engine\nav::cacheFinalCheck();

//
\snkeng\core\site\amp::printPage([
	//
	'title' => $contents['title'],
	'metaWord' => $contents['metaWord'],
	'metaDesc' => $contents['metaDesc'],
	'type' => 'Article',
	'twType' => 'Article',
	'urlCanonical' => $contents['urlCanonical'],
	'image' => $contents['image'],
	'content_mode' => $contentMode,
	//
	'metaData' => <<<JSON
{
	"@context":"http://schema.org",
	"@type":"NewsArticle",
	"mainEntityOfPage":"http://cdn.ampproject.org/article-metadata.html",
	"headline":"{$contents['title']}",
	"datePublished":"{$contents['dtpub_iso']}",
	"dateModified":"{$contents['dtmod_iso']}",
	"description":"{$contents['metaDesc']}",
	"author":{
		"@type":"Person",
		"name":"{$contents['username']}"
	},
	"publisher":{
		"@type":"Organization",
		"name":"{$siteVars['site']['name']}",
		"logo":{
			"@type":"ImageObject",
			"url":"{$siteVars['site']['surl']}{$siteVars['site']['img']}"
		}
	},
	$imageMetaData
}
JSON
	,
	//
	'content_style' => <<<CSS
.b_properties { display:flex; justify-content:space-between; margin-bottom:20px; }
.headContent { margin-bottom:20px; }
.adv_text { text-align:justify; line-height:1.8em; }
.adv_text img { display:block; clear:both; float:none; width:100%; }
.adv_text img.gOIB { margin:0 0 10px 0; }
.adv_text img.gOIC { margin:0 auto 10px auto; }
.adv_text img.gOIL { margin:0 10px 10px 0; float:left; }
.adv_text img.gOIR { margin:0 0 10px 10px; float:right; }
.adv_text p { margin-bottom:1em; margin-top:1em; }
.adv_text h1, .adv_text h2, .adv_text h3, .adv_text h4, .adv_text h5, .adv_text h6 { margin-bottom:1em; margin-top:1.5em; line-height:1.3em; }
.adv_text h1 { font-size:1.8em; font-weight:bold; }
.adv_text h2 { font-size:1.6em; font-weight:bold; }
.adv_text h3 { font-size:1.5em; font-style:italic; }
.adv_text h4 { font-size:1.4em; font-weight:bold; text-decoration:underline; }
.adv_text h5 { font-size:1.3em; font-style:italic; }
.adv_text h6 { font-size:1.2em; text-decoration:underline; font-weight:normal; }
.adv_text .videoWrapper { margin:10px 0; }
.adv_text figure { margin:10px 0; background-color:#F5F5F5; }
.adv_text figcaption { display:block; margin:0 10px; }
.adv_text picture { display:block; width:100%; }
.adv_text figure.quote { background-color:#F5F5F5; border-radius:5px; padding:10px; font-style:italic; color:#000; }
.adv_text figure blockquote { border-bottom:1px solid #666; margin:0 0 10px 0; padding:0 0 10px 0; }
.adv_text figure.quote figcaption { text-align:left; font-weight:bold; }
.adv_text figure.quote figcaption:before { content:'-'; }
.error { display:block; border:1px solid #900; padding:20px; margin:10px 0; }
.note { display:block; border:1px solid #111; }
.note > .title { display:block; padding:10px; border-bottom:1px solid #111; background-color:#000; color:#FFF; }
.note > .content { display:block; padding:10px; }
amp-youtube { margin:10px 0; }
.notification { display:flex; overflow:auto; background:#DFDFDF; color:#444; padding:10px; margin:0 0 10px 0; border:1px #666 solid; border-radius:3px; }
.notification.light { background-color:#F0F0F0; border-color:#444; }
.notification.success { background-color:#a6ffa5; border-color:#0f0; }
.notification.info { background-color:#b7cfff; border-color:#335dff; }
.notification.warning { border-color:#F8FF00; background-color:#FBFF6E; }
.notification.alert { background-color:#ff7575; border-color:#f00; color:#222; }
.alert:empty, .success:empty, .warning:empty, .notification:empty, .description:empty { display:none; }
.notification > .icon { display:inline-flex; align-items:center; justify-content:center; border-radius:50%; height:40px; width:40px; padding:5px; background-color:#333; color:#fff; margin-right:10px; }
.notification > .icon > svg { width:24px; height:24px; fill:currentColor; }
.notification.success .icon { background-color:#019f0e; }
.notification.info .icon { background-color:#2335c1; }
.notification.warning .icon { background-color:#ffbe00; }
.notification.alert .icon { background-color:#c40003; }
.notification .text { -ms-flex:1; flex:1; }
.notification .title { display:block; font-weight:bold; margin-bottom:5px; }
.notification .content { white-space:pre-wrap; }
.notification .actions { display:flex; margin-top:5px; }
.notification .actions button { padding:5px 10px; border-radius:2px; }
CSS
	,
	'svg_icons' => [
		['fa-clock-o', '0 0 24 24', '<path d="M12,20A8,8 0 0,0 20,12A8,8 0 0,0 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22C6.47,22 2,17.5 2,12A10,10 0 0,1 12,2M12.5,7V12.25L17,14.92L16.25,16.15L11,13V7H12.5Z" />'],
		['fa-folder-o', '0,0,1664,1792', '<path d="M1536,1312l0-704q0-40-28-68t-68-28l-704,0q-40,0-68-28t-28-68l0-64q0-40-28-68t-68-28l-320,0q-40,0-68,28t-28,68l0,960q0,40,28,68t68,28l1216,0q40,0,68-28t28-68z M1664,608l0,704q0,92-66,158t-158,66l-1216,0q-92,0-158-66t-66-158l0-960q0-92,66-158t158-66l320,0q92,0,158,66t66,158l0,32l672,0q92,0,158,66t66,158z" />'],
	],
	//
	'content_body' => <<<HTML
<article>

<h1 class="title">{$contents['title']}</h1>

<div class="b_properties">
	<div class="b_time">
		<svg class="icon"><use xlink:href="#fa-clock-o" /></svg>
		<span>{$contents['dtpub_simple']}</span>
	</div>
	<div class="b_info">
		<a class="category" target="_blank" href="{$siteVars['server']['base_path']}/blog/category/{$contents['curlname']}/">
			<svg class="icon"><use xlink:href="#fa-folder-o" /></svg>
			<span>{$contents['cname']}</span>
		</a>
	</div>
</div>

{$contents['elHead']}

<div class="content">
<!-- INI: ArtBody -->
<div class="adv_text">
{$postContent}
</div>
<!-- END: ArtBody -->
</div>

</article>
HTML
	,
	//
]
);
//

exit();
