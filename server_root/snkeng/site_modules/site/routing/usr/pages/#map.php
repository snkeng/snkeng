<?php
$conditions['app']['name'] = 'site';

// debugVariable($conditions['app']['site']);

$siteVars['page']['main'] = '';
$siteVars['page']['api'] = '/api/module_usr/site';

//
$firstParam = ( \snkeng\core\engine\nav::current() === $conditions['app']['site']['settings']['blog']['name'] ) ? 'blog' : \snkeng\core\engine\nav::current();

//
switch ( $firstParam ) {
	//
	case 'docs':
		require __DIR__ . '/docs_#map.php';
		break;

	//
	case 'post':
	case 'videos':
	case 'blog':
		//
		$siteVars['page']['main'].= '/'.$firstParam;
		$siteVars['page']['api'].= '/blog';

		//
		require __DIR__ . '/blog_#map.php';
		break;

	//
	case 'contact':
		require __DIR__ . '/contact.php';
		break;

	//
	default:
		$cUrl = $_SERVER['REQUEST_URI'];
		// Remover parámetros get
		if ( strpos($cUrl, '?') !== false ) {
			$cUrl = substr($cUrl, 0, strpos($cUrl, '?'));
		}

		// Ajustar si es async
		if ( \snkeng\core\engine\nav::$async ) {
			$cUrl = substr($cUrl, strpos($cUrl, '/', 9));
		}

		// Security
		$cUrl = preg_replace('/[^A-Za-z0-9\/\-_]/', '', $cUrl);

		// Revisar que exista contenido
		if ( empty($cUrl) ) { \snkeng\core\engine\nav::invalidPage(); }

		// Obtener página a partir del nombre
		$sql_qry = <<<SQL
SELECT
	art_id AS id, art_title AS title, art_urltitle AS urlTitle, art_url AS fullUrl,
	art_dt_mod AS dtMod,
    art_type_secondary AS typeSub, art_content_extra_b AS extraBasic,
    art_content_header_content AS contentHeaderContent, art_content_header_type AS contentHeaderType,
    art_content_body_component AS contentBodyComponent, art_content_bundles AS contentBundles,
	art_meta_word AS metaWord, art_meta_desc AS metaDesc, art_content AS content,
	art_img_alt_text imageAltText, art_img_struct AS imageStruct, art_img_width AS imageWidth, art_img_heigth AS imageHeight
FROM sc_site_articles
WHERE art_url='{$cUrl}' AND art_type_primary='page'
LIMIT 1;
SQL;
		$params['page'] = \snkeng\core\engine\mysql::singleRowAssoc(
			$sql_qry,
			[],
			[
				'errorKey' => 'AppSitesMap01',
				'errorDesc' => 'No fue posible revisar por sitios externos'
			]
		);

		// Hay datos del sitio?
		if ( empty($params['page']) ) { \snkeng\core\engine\nav::invalidPage(); }

		// debugVariable($params['page']);

		// Redirection page?
		if ( $params['page']['typeSub'] === 'redir' ) {
			$extraLink = '';

			// Check if it is a partial page
			if ( substr( $_SERVER['REQUEST_URI'], 0, 8) === '/partial' ) {
				$extraLink = substr( $_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '/', 9));
			}

			// Redirect
			header("Location: {$extraLink}{$params['page']['extraBasic']}", true, 307);
			exit();
		}

		// Ejecutar página
		require __DIR__ . '/page_simple.php';
		break;
}
//
