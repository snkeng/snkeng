<?php
// BLOG - Navigation (many)

\snkeng\core\engine\nav::pageFileModuleAdd('core', '', '/components-simple/se-async-content.mjs');
\snkeng\core\engine\nav::pageFileModuleAdd('core', '', '/components-simple/se-async-content.css');
\snkeng\core\engine\nav::pageFileModuleAdd('site_modules', 'site', '/pages/blog/nav.css');
\snkeng\core\engine\nav::pageFileModuleAdd('site_modules', 'site', '/pages/blog/sidebar.css');


// Cache final check
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();

//
$qryWhere = [];
$typeXtra = '';
switch ( $params['type'] )
{
	// Autor
	case 'author':
		// Títulos
		$page['head']['title'] = "Posts by: " . $params['data']['username'];
		$page['head']['metaWord'] = "";
		$page['head']['metaDesc'].= $params['extraData']['desc'];
		$page['head']['url'] = $siteVars['site']['url']."/author/{$params['data']['userurl']}/";
		// Autor
		$an_qry['where']['author']['set'] = $params['data']['id'];

		$qryWhere['userId'] = $params['data']['id'];

		//
		$userTw = ( true ) ? '' : '';

		//
		$typeXtra = <<<HTML
<div class="blog_user">
	<img class="userImg" src="/res/image/objects/w_160/{$params['data']['image']}" />
	<div class="uData">
		<div class="title">{$params['data']['username']}</div>
		<div class="desc">{$params['extraData']['desc']}</div>
		<div class="socnet">{$userTw}</div>
	</div>
</div>\n
HTML;
		break;

	// Categoría
	case 'category':
		// Extraer variables
		$sql_qry = "SELECT
						cat_title AS ctitle, cat_urltitle AS cutitle
					FROM sc_site_category
					WHERE cat_id={$params['catId']}
					LIMIT 1;";
		$curObj = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [], [
			'errorKey' => 'cat',
			'errorDesc' => 'No fue posible leer la categoría'
		]);

		// Títulos
		$page['head']['title'] = "Categoría ".$curObj['ctitle'];
		$page['head']['metaWord'] = "";
		$page['head']['metaDesc'].= "";
		$page['head']['url'] = $siteVars['site']['url']."/blog/category/{$curObj['cutitle']}/";
		//
		$qryWhere['catId'] = $params['catId'];
		//
		$typeXtra = <<<HTML
<h2>{$postLang['category']}: {$curObj['ctitle']}</h2>\n
HTML;
		break;

	// Búsqueda
	case 'search':
		// Títulos
		$page['head']['title'] = "Búsqueda ".$params['s_words'];
		$page['head']['metaWord'] = "";
		$page['head']['metaDesc'].= "";
		$page['head']['url'] = $siteVars['site']['url']."/buscar/";
		//
		$qryWhere['search'] = $params['s_words'];
		//
		$typeXtra = <<<HTML
<h2>{$postLang['search']}: {$params['s_words']}</h2>\n
HTML;
		break;

	// Búsqueda
	case 'tags':
		//
		$sql_qry = <<<SQL
SELECT
cat_title AS ctitle, cat_urltitle AS cutitle
FROM sc_site_category
WHERE cat_id={$siteVars['catid']}
LIMIT 1;
SQL;
		$curObj = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [], [
			'errorKey' => '',
			'errorDesc' => ''
		]);

		$qryWhere['tagId'] = $params['tagId'];

		// Títulos
		$page['head']['title'] = "Tag ".$curObj['ctitle'];
		$page['head']['metaWord'] = "";
		$page['head']['metaDesc'].= "";
		$page['head']['url'] = $siteVars['site']['url']."/tags/";

		//
		$typeXtra = <<<HTML
<h2>{$postLang['tag']}: {$curObj['cutitle']}</h2>\n
HTML;
		break;

	// Sin modificaciones
	default:
		// Títulos
		$page['head']['title'] = "Blog";
		$page['head']['metaWord'] = "Blog";
		$page['head']['metaDesc'].= "Blog";
		$page['head']['url'] = $siteVars['site']['url']."/{$conditions['app']['site']['settings']['blog']['name']}/";
		break;
}

//
// SIDEBAR
//
$sideBarHTML = \snkeng\core\site\sideBar::createSidebar([
	['name'=>'blogSearch', 'props'=>''],
	['name'=>'blogMosts', 'props'=>''],
	['name'=>'blogCategory', 'props'=>''],
	['name'=>'banner', 'props'=> [ 'size' => 1 ] ],
]);

// Comentarios
/*
$comments = '';
switch ($siteVars['modset']['blog']['settings']['comments'])
{
	// Socnet
	case 1:
		$comments = '<div class="sep"><svg class="icon inline"><use xlink:href="#fa-comments" /></svg> <a se-nav="se_middle" href="{$siteVars['server']['base_path']}/blog/post/!pid;/!purltitle;/#comments">!msgcount; comentarios</a></div>';
		break;
	// Disqus
	case 2:
		if (!$_ENV['SE_DEBUG'])
		{
			$page['js'] .= "var disqus_shortname ='{$siteVars['extmod']['disqus']['shortname']}';\n";
			se.api.jsLoad('//' + $siteVars['extmod']['disqus']['shortname'] + '.disqus.com/count.js');
			$comments = '<div class="sep"><svg class="icon inline"><use xlink:href="#fa-comments" /></svg> <a se-nav="se_middle" href="{$siteVars['server']['base_path']}/blog/post/!pid;/!purltitle;/#disqus_thread" data-disqus-identifier="b_' . $params['pId'] . '">Comentarios</a></div>';
		} else {
			$comments = '<div class="sep"><svg class="icon inline"><use xlink:href="#fa-comments" /></svg> <a se-nav="se_middle" href="{$siteVars['server']['base_path']}/blog/post/!pid;/!purltitle;/#comments">Disqus no disponible en prueba</a></div>';
		}
		break;
	// Facebook <fb:comments-count href=http://example.com/></fb:comments-count>
	case 3:
		if (!$_ENV['SE_DEBUG']) {
			$comments = '<div class="sep"><svg class="icon inline"><use xlink:href="#fa-comments" /></svg> <a se-nav="se_middle" href="{$siteVars['server']['base_path']}/blog/post/!pid;/!purltitle;/#comments"><fb:comments-count href="'.$siteVars['server']['url'].'/blog/post/!pid;/!purltitle;/"></fb:comments-count> comentarios</a></div>';
		} else {
			$comments = '<div class="sep"><svg class="icon inline"><use xlink:href="#fa-comments" /></svg> <a se-nav="se_middle" href="{$siteVars['server']['base_path']}/blog/post/!pid;/!purltitle;/#comments">FB no disponible en prueba</a></div>';
		}

		break;
	default:
		break;
}
$author = '';
if ($siteVars['modset']['blog']['settings']['bloggerFocus'] === 1 )
{
	$author = '<div class="sep"><svg class="icon inline"><use xlink:href="#fa-user" /></svg> <a se-nav="se_middle" href="{$siteVars['server']['base_path']}/autor/!burlname;/" itemprop="author">!bname;</a></div>';
}
*/

//
$qryWhereStr = '';
foreach ( $qryWhere as $name => $value ) {
	$encValue = addslashes($value);
	$qryWhereStr.= " data-where:{$name}='{$encValue}'";
}

// Página
$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">{$postLang['type']}</div></div>
<!-- INI:BLOG -->
<div class="template_grid_borders">
<div class="template_grid_12">
<!-- INI:POSTS -->
<div class="span-09 push-01">
{$typeXtra}

<se-async-content url="/api/module_usr/site/blog/post/readAll" scrolls="4"{$qryWhereStr}>
	<template>
<article class="postMini status!published;" itemtype="http://schema.org/Article" data-ajsel="object" data-objid="!pid;" data-objtitle="!ptitle;">
	<div class="body">
		<div>
		<a class="imgCont" se-nav="se_middle" href="!urlFull;">
			<picture itemprop="image">
				<source type="image/webp" srcset="/res/image/site/w_320/!image;.webp" /> 
				<img src="/res/image/site/w_320/!image;.jpg" />
			</picture>
		</a>
		</div>
		<div class="contentSmall">
			<a class="category" se-nav="se_middle" href="/blog/category/!curlname;/">!ctitle;</a>
			<a class="title" se-nav="se_middle" href="!urlFull;" itemprop="name">!ptitle;</a>
			<div class="desc" itemprop="description">!mincontent;</div>
			<div class="info" title="!dtpub;"><svg class="icon inline mr"><use xlink:href="#fa-clock-o"></use></svg>!dtpub;</div>
		</div>
	</div>
</article>
	</template>
	<div data-type="container"></div>
	<div class="isLoading"><svg class="icon spin"><use xlink:href="#fa-spinner" /></svg></div>
	<button class="loadMore">Cargar más</button>
</se-async-content>

</div>
<!-- END:POSTS -->
<!-- INI:SIDEBAR -->
<div class="span-03 push-10 hidden_tablet">\n
{$sideBarHTML}
</div>
<!-- END:SIDEBAR -->
</div>
</div>
<!-- END:BLOG -->\n
HTML;
//
