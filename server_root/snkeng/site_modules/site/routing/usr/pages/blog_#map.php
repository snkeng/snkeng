<?php
// Load defaults
\snkeng\core\engine\nav::pageFileModuleAdd('site_modules', 'site', '/pages/blog/sidebar.css');

//
$postLang = (require __DIR__ . '/../../../lang/' .$siteVars['site']['lang'].'.php');

// Links FIX FOR FALLBACK
if ( \snkeng\core\engine\nav::current() !== 'post' ) {
	\snkeng\core\engine\nav::next(); // Skip blog
}

//
switch ( \snkeng\core\engine\nav::next() ) {
	// autor
	case 'author':
		// Definir y revisar variables
		$autorName = preg_replace('/[^A-z0-9\-_]/', '', \snkeng\core\engine\nav::next());

		//
		if ( empty($autorName) ) { \snkeng\core\engine\nav::invalidPage(); }

		// Aprobar

		// Obtener el Id y hacer las operaciones
		$sql_qry = <<<SQL
SELECT
	a_id AS id,
	a_obj_name AS username, a_url_name_full AS userurl,
	a_img_crop AS image
FROM sb_objects_obj
WHERE a_url_name_full='{$autorName}'
LIMIT 1;
SQL;
		$authorData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);

		//
		if ( empty($authorData) ) { \snkeng\core\engine\nav::invalidPage(); }

		// Read user settings
		$userData = \snkeng\core\socnet\object_properties::readArray($authorData['id'], 'snkeng', 'profile');
		if ( empty($userData) ) { se_killWithError('User information not set'); }
		$userData = $userData['data'];

		//
		$params = [
			'type' => 'author',
			'data' => $authorData,
			'extraData' => $userData
		];

		require __DIR__ . '/blog_#many.php';
		break;

	// categorías
	case 'category':
		// Definir y revisar variables
		$catName = preg_replace('/[^A-z0-9\-_]/', '', \snkeng\core\engine\nav::next());

		// Aprobar
		if ( empty($catName) ) { \snkeng\core\engine\nav::invalidPage(); }

		// Obtener el Id y hacer las operaciones
		$sql_qry = "SELECT cat_id
					FROM sc_site_category
					WHERE cat_urltitle='{$catName}'
					LIMIT 1;";
		$catId = \snkeng\core\engine\mysql::singleNumber($sql_qry);

		//
		if ( empty($catId) ) { \snkeng\core\engine\nav::invalidPage(); }

		$params = ['type' => 'category', 'catId' => $catId];
		require __DIR__ . '/blog_#many.php';
		break;

	// Tags
	case 'tag':
		// Definir y revisar variables
		$tagName = preg_replace('/[^A-z0-9\-_]/', '', \snkeng\core\engine\nav::next());

		//
		if ( empty($tagName) ) { \snkeng\core\engine\nav::invalidPage(); }

		// Obtener el Id y hacer las operaciones
		$sql_qry = "SELECT tag_id
					FROM sc_site_tags
					WHERE tag_urltitle='{$tagName}'
					LIMIT 1;";
		$tagId = \snkeng\core\engine\mysql::singleNumber($sql_qry);

		//
		if ( empty($tagId) ) { \snkeng\core\engine\nav::invalidPage(); }

		//
		$params = ['type' => 'tags', 'tagId' => $tagId];
		require __DIR__ . '/blog_#many.php';
		break;

	// videos
	case 'videos':
		$params = ['type' => 'videos'];
		require __DIR__ . '/blog_#many.php';
		break;

	// Buscar
	case 'search':
		$params = [
			'type' => 'search',
			's_words' => $_GET['sT']
		];
		require __DIR__ . '/blog_#many.php';
		break;

	//
	case '':
	case null:
		// Mostrar todos
		$params = ['type'=>''];
		require __DIR__ . '/blog_#many.php';
		break;

	// Feed
	case 'feed':
		$rssType = ( \snkeng\core\engine\nav::next() === 'atom' ) ? 'atom' : 'rss2';
		\snkeng\core\site\blogFeed::printFeed($rssType);
		break;

	// Error
	default:
		$urlTitle = \snkeng\core\engine\nav::current();

		//
		$pData = explode('-', $urlTitle);
		$pId = intval(end($pData));

		//
		if ( empty($pId) ) { \snkeng\core\engine\nav::invalidPage(); }

		//
		$sql_qry = <<<SQL
SELECT
	art_id AS pId, art_urltitle AS urlTitle, art_url AS fullUrl, art_type_primary AS type, art_dt_mod AS dtMod
FROM sc_site_articles
WHERE art_id={$pId}
LIMIT 1;
SQL;

		$params['page'] = \snkeng\core\engine\mysql::singleRowAssoc(
			$sql_qry,
			[
				'int' => ['pId']
			]
		);

		//
		if ( empty($params['page']) || $params['page']['type'] !== 'blog' ) { \snkeng\core\engine\nav::invalidPage(); }

		\snkeng\core\engine\nav::cacheCheckDate($params['page']['dtMod']);


		//
		if ( $params['page']['fullUrl'] !== \snkeng\core\engine\nav::$pathStr ) {
			header('Location: ' . $siteVars['server']['url'] . $params['page']['fullUrl']);
			exit();
		}

		//
		$siteVars['page']['api'].= '/post/' . $params['page']['pId'];

		//
		if ( isset($_GET['amp']) && $_GET['amp'] ) {
			require __DIR__ . '/blog_[id]_amp.php';
			exit();
		} else {
			require __DIR__ . '/blog_[id]_normal.php';
		}
		break;
}
//
