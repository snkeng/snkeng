<?php
// DOCS - Page
// Contador de visitas simple

// Add bundles?
if ( !empty($params['docData']['contentBundles']) ) {
	$bundles = explode(',', $params['docData']['contentBundles']);
	\snkeng\core\engine\nav::pageFileGroupAdd($bundles);
}


// Cache final check
\snkeng\core\engine\nav::cacheCheckDate($params['page']['dtMod']);
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheFinalCheck();



if ( !\snkeng\core\engine\login::check_loginLevel('admin') && empty($_SESSION['views']['art'][$params['page']['id']]) ) {
	$_SESSION['views']['art'][$params['page']['id']] = 1;
	$sql_qry = "UPDATE sc_site_articles SET art_count_views=art_count_views+1 WHERE art_id={$params['page']['id']} LIMIT 1;";
	if ( !\snkeng\core\engine\mysql::submitQuery($sql_qry) ) { echo("ERROR (SISTEMA): contador de visitas."); }
}

// Sitio General
$page['head']['title'] = $params['page']['title'];
$page['head']['metaWord'] = $params['page']['metaWord'];
$page['head']['metaDesc'] = $params['page']['metaDesc'];
// URL
$page['url_path'] = $params['page']['fullUrl'];
$page['head']['url'] = $siteVars['site']['url'].$params['page']['fullUrl'];
//
// $page['head']['og']['img'] = $siteVars['site']['url'].$params['page']['image']."_w832.jpg";

// AMP Support
$page['head']['extra'][] = [
	'type' => 'link',
	'content' => "rel='amphtml' href='{$siteVars['site']['url']}{$params['page']['fullUrl']}?amp=1'"
];

// Breadcrumbs
$breadCrumbs = '';
$topNavigation ='';
$nextUrl = '';
$prevUrl = '';
$roots = str_split($params['page']['hierarchy'], 4);
$cIndex = '';
$pageIndex = base_convert(array_pop($roots), 36, 10);
if ( count($roots) > 0 ) {
	//
	$count = count($roots);
	$cLink = [];
	for ( $i = 0; $i < $count; $i++ ) {
		$cIndex.= $roots[$i];
		$cLink[] = $cIndex;
	}
	$cList = implode("','", $cLink);
	$sql_qry = <<<SQL
SELECT
art.art_id AS id, art.art_title AS title, art.art_url AS fullUrl
FROM sc_site_articles AS art
WHERE art.art_type_primary='docs' AND art.cat_id='{$params['page']['catId']}' AND art.art_order_hierarchy IN ('{$cList}')
ORDER BY art.art_order_hierarchy ASC
LIMIT {$count};
SQL;

	$struct = <<<HTML
<a se-nav="se_middle" href="!fullUrl;">!title;</a>
HTML;

	$breadCrumbs = \snkeng\core\engine\mysql::printSimpleQuery($sql_qry, $struct);
}

// Sibings
// $prevIndex = $cIndex.str_pad(base_convert($pageIndex - 1, 10, 36), 4, '0', STR_PAD_LEFT);
$sql_qry = <<<SQL
SELECT
art.art_id AS id, art.art_title AS title, art.art_url AS fullUrl
FROM sc_site_articles AS art
WHERE art.art_type_primary='docs' AND art.cat_id='{$params['page']['catId']}' AND art.art_order_hierarchy<'{$params['page']['hierarchy']}'
ORDER BY art.art_order_hierarchy DESC
LIMIT 1;
SQL;

$prevData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);
if ( $prevData ) {
	$prevUrl = $prevData['fullUrl'];
	$topNavigation.= <<<HTML
<a se-nav="se_middle" href="{$prevData['fullUrl']}">« {$prevData['title']}</a>
HTML;
	//
}



//
// $nextIndex = $cIndex.str_pad(base_convert($pageIndex + 1, 10, 36), 4, '0', STR_PAD_LEFT);
$sql_qry = <<<SQL
SELECT
art.art_id AS id, art.art_title AS title, art.art_url AS fullUrl
FROM sc_site_articles AS art
WHERE art.art_type_primary='docs' AND art.cat_id='{$params['page']['catId']}' AND art.art_order_hierarchy>'{$params['page']['hierarchy']}'
ORDER BY art.art_order_hierarchy ASC
LIMIT 1;
SQL;

$nextData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry);
if ( $nextData ) {
	$nextUrl = $nextData['fullUrl'];
	$topNavigation.= <<<HTML
<a se-nav="se_middle" href="{$nextData['fullUrl']}">{$nextData['title']} »</a>
HTML;
	//

}

// Lateral navigation
$relHierarchy = substr($params['page']['hierarchy'],0, -4);
// debugVariable($relHierarchy);

$level_reduction = $params['page']['orderLvl'] - 1;

// Hijos
$sql_qry = <<<SQL
SELECT
art.art_id AS id,
art.art_title AS title, art.art_urltitle AS urlTitle, art.art_url AS fullUrl, art.art_dt_mod AS dtMod,
CONV(art.art_order_current , 36, 10) AS orderCur, (art.art_order_level-{$level_reduction}) AS orderLvl,
art.art_order_hierarchy AS orderHierarchy,
IF(art.art_order_hierarchy='{$params['page']['hierarchy']}', 1, 0) AS curPage
FROM sc_site_articles AS art
WHERE
    art.art_type_primary='docs' AND
    art.cat_id='{$params['page']['catId']}' AND
    art.art_status_published=1 AND (
	    ( art.art_order_hierarchy LIKE "{$relHierarchy}_%" AND art.art_order_level<{$params['page']['orderLvl']} + 1 ) OR
	    ( art.art_order_hierarchy LIKE "{$params['page']['hierarchy']}_%" AND art.art_order_level<{$params['page']['orderLvl']} + 2 )
    )
ORDER BY art.art_order_hierarchy ASC
LIMIT 50;
SQL;

$struct = <<<HTML
<a se-nav="se_middle" data-level="!orderLvl;" data-order="!orderCur;" href="!fullUrl;" data-current="!curPage;">!title;</a>
HTML;

$pages_children = \snkeng\core\engine\mysql::printSimpleQuery($sql_qry, $struct);


//
$params['docData']['contentBodyMod'] = $params['docData']['contentBodyMod'] ?? 'div';


// $params['page']['content'] = stripcslashes($params['page']['content']);

// ======================================
// Imprimir
// ======================================
//
$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">Documentation</div></div>
<!-- INI:Content -->
<div class="wpContent">
<div class="se_menu_top hidden_mobile">
<div class="breadcrumbs"><a se-nav="se_middle" href="/docs/{$params['docData']['urlTitle']}">{$params['docData']['title']}</a>{$breadCrumbs}</div>
<div class="normal">{$topNavigation}</div>
</div>
<div class="grid">
<div class="gr_sz03">
	<div class="docs_mobile_nav">
		<button class="navBut" onclick="$('#docSideMenu').se_classToggle('active')"><svg class="icon inline"><use xlink:href="#fa-navicon" /></svg></button>
		<div>
			<a class="navBut" se-nav="se_middle" href="{$prevUrl}"><svg class="icon inline"><use xlink:href="#fa-arrow-left" /></svg></a>
			<a class="navBut" se-nav="se_middle" href="{$nextUrl}"><svg class="icon inline"><use xlink:href="#fa-arrow-right" /></svg></a>
		</div>
	</div>
	<div id="docSideMenu" class="docs_side_menu">
		<div class="show_mobile breadcrumbs_mobile">{$breadCrumbs}</div>
		<div class="side_nav">
			<div class="mini_title">Other sections</div>
			<div class="docsMenu docs">{$pages_children}</div>
		</div>
		<div class="side_nav">
			<div class="mini_title">In this page</div>
			<div class="docsMenu docs">{$params['page']['contentExtra']}</div>
		</div>
	</div>
</div>\n
<div class="gr_sz09" itemscope itemtype="http://schema.org/Article">\n
<div id="documentPost" class="doc">
	<article>
	<div class="title" itemprop="name">{$params['page']['title']}</div>
	<div class="content">
<!-- INI: ArtBody -->
<{$params['docData']['contentBodyMod']} class="adv_text" itemprop="articleBody">
{$params['page']['content']}
</{$params['docData']['contentBodyMod']}>
<!-- END: ArtBody -->
	</div>
	</article>
</div>
</div>\n
</div>
</div>
<!-- END:Content -->\n\n
HTML;
//
