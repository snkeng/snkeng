<?php
// Cache check

//
\snkeng\core\engine\nav::cacheCheckFile(__FILE__);
\snkeng\core\engine\nav::cacheCheckDate($params['page']['dtMod']);
\snkeng\core\engine\nav::cacheFinalCheck();

// Adición de título, desc y tags.

$page['head']['title'] = $params['page']['title'];
$page['head']['metaWord'] = $params['page']['metaWord'];
$page['head']['metaDesc'].= $params['page']['metaDesc'];
$page['head']['url'] = $params['page']['fullUrl'];
$page['head']['og']['type'] = 'website';

// Add bundles?
if ( !empty($params['page']['contentBundles']) ) {
	$bundles = explode(',', $params['page']['contentBundles']);
	\snkeng\core\engine\nav::pageFileGroupAdd($bundles);
}


// debugVariable($params['page']);
if ( !empty($params['page']['imageStruct']) ) {
	$page['head']['og']['img'] = $siteVars['server']['url'].'/res/image/site/w_720/'.$params['page']['imageStruct'].'.jpg';
}

$cPage = [
	'title' => '',
	'header' => '',
	'bodyContainer' => ( !empty($params['page']['contentBodyComponent']) ) ? $params['page']['contentBodyComponent'] : 'div'
];

//
switch ( $params['page']['contentHeaderType'] ) {
	//
	case 'image':
		// Only add structure if present
		if ( !empty($params['page']['imageStruct']) ) {
			//
			$cPage['header'] = <<<HTML
<div class="pageCont pageMediaContainer">
	<picture itemprop="image">
		<source type="image/webp"
			srcset="/res/image/site/w_360/{$params['page']['imageStruct']}.webp 360w,
					/res/image/site/w_720/{$params['page']['imageStruct']}.webp 720w,
					/res/image/site/w_1200/{$params['page']['imageStruct']}.webp 1200w"
			sizes="(max-width:768px) 100vw, 80vw"
		/>
		<img class="mainImg" alt="{$params['page']['imageAltText']}" width="{$params['page']['imageWidth']}" height="{$params['page']['imageHeight']}"
			src="/res/image/site/w_1200/{$params['page']['imageStruct']}.jpg"
			srcset="/res/image/site/w_360/{$params['page']['imageStruct']}.jpg 360w,
					/res/image/site/w_720/{$params['page']['imageStruct']}.jpg 720w,
					/res/image/site/w_1200/{$params['page']['imageStruct']}.jpg 1200w"
			sizes="(max-width:768px) 100vw, 80vw"
		/>
	</picture>
</div>
HTML;
			//
		}
		break;

	//
	case 'video':

		break;

	//
	case '':
	case 'text':
	case 'normal':
		break;

	//
	default:
		se_killWithError('caso no programado');
		break;
}

//
$page['body'] = <<<HTML
<div class="fullTitle"><div class="wpContent">{$params['page']['title']}</div></div>
{$cPage['header']}
<!-- INI:PAGE CONTENT -->
<{$cPage['bodyContainer']} class="pageSimple_text adv_text">
{$params['page']['content']}
</{$cPage['bodyContainer']}>
<!-- END:PAGE CONTENT -->\n
HTML;
//
