<?php
//
switch ( \snkeng\core\engine\nav::next() )
{
	//
	case 'blog':
		require __DIR__ . '/blog.php';
		break;

	//
	case 'contact':
		require __DIR__ . '/contact.php';
		break;

	//
	case 'banner':
		require __DIR__ . '/banner.php';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
