<?php
//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'post':
		if ( !is_numeric(\snkeng\core\engine\nav::next(false)) ) {
			//
			switch ( \snkeng\core\engine\nav::next() ) {
				//
				case 'readAll':
					$an_qry = (require __DIR__ . '/../../../nav/blog.php');
					// Preparar estructura
					\snkeng\core\engine\nav::cacheSetTime(3600);
					\snkeng\core\general\asyncDataJson::printJson($an_qry, 'GET');
					break;

				//
				default:
					\snkeng\core\engine\nav::invalidPage();
					break;
			}
		} else {
			//
			$params['vars']['postId'] = intval(\snkeng\core\engine\nav::next());
			//
			switch ( \snkeng\core\engine\nav::next() ) {
				//
				case 'like':
					$params['vote'] = strval($_POST['vote']);
					if ( empty($params['vote']) ) {
						se_killWithError("Incomplete data","pId:{$params['vars']['postId']};vote:{$params['vote']};");
					}

					//
					if ( !empty($_SESSION['votes']['p'][$params['vars']['postId']]) ) {
						se_killWithError("Voto ya registrado");
					}

					//
					switch ( $params['vote'] ) {
						case 'p':
							$sql_qry = "UPDATE sc_site_articles SET art_count_likes=art_count_likes+1 WHERE art_id={$params['vars']['postId']} LIMIT 1;";
							break;
						case 'n':
							$sql_qry = "UPDATE sc_site_articles SET art_count_dislikes=art_count_dislikes+1 WHERE art_id={$params['vars']['postId']} LIMIT 1;";
							break;
						default:
							se_killWithError("Voto no válido.", $params['vote']);
							break;
					}

					\snkeng\core\engine\mysql::submitQuery($sql_qry, [
						'errorKey' => 'postUserLike',
						'errorDesc' => 'No se pudo actualizar el contador del post.'
					]);

					// Asignar y evitar impresión
					$_SESSION['votes']['p'][$params['vars']['postId']] = 1;
					$sql_qry = "SELECT art_count_likes, art_count_dislikes FROM sc_site_articles WHERE art_id={$params['vars']['postId']};";
					$data = \snkeng\core\engine\mysql::singleRow($sql_qry, [
						'errorKey' => 'postUserLike',
						'errorDesc' => 'No se pudo leer el contador del post.'
					]);

					//
					$response['d'] = "<span class='likeBtn'><svg class='icon inline mr'><use xlink:href='#fa-thumbs-o-up' /></svg>({$data[0]})</span> <span class='likeBtn'><svg class='icon inline mr'><use xlink:href='#fa-thumbs-o-down' /></svg>({$data[1]})</span>";
					break;

				//
				default:
					\snkeng\core\engine\nav::invalidPage();
					break;
			}
		}
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
