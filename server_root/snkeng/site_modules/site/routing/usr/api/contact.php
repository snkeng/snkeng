<?php
//
switch ( \snkeng\core\engine\nav::next() ) {
	//
	case 'mail':
		// Revisar anti bot
		if ( !\snkeng\core\external\recaptcha::verify() ) {
			se_killWithError("Captcha no aprobado.");
		}

		// Revisar operación per se
		$rData = \snkeng\core\general\saveData::postParsing(
			$response,
			[
				'uName' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 50, 'filter' => 'simpleTitle', 'process' => false, 'validate' => true],
				'lName' => ['name' => 'Nombre', 'type' => 'str', 'lMin' => 0, 'lMax' => 50, 'filter' => 'simpleTitle', 'process' => false, 'validate' => false],
				'eMail' => ['name' => 'Email', 'type' => 'str', 'lMin' => 0, 'lMax' => 75, 'filter' => 'email', 'process' => false, 'validate' => true],
				'uTel' => ['name' => 'Teléfono', 'type' => 'str', 'lMin' => 0, 'lMax' => 20, 'filter' => '', 'process' => false, 'validate' => false],
				'ref' => ['name' => 'URL Anterior', 'type' => 'str', 'lMin' => 0, 'lMax' => 150, 'filter' => 'url', 'process' => false, 'validate' => false],
				'uMsg' => ['name' => 'Mensaje', 'type' => 'str', 'lMin' => 0, 'lMax' => 500, 'filter' => 'simpleText', 'process' => false, 'validate' => false]
			]
		);

		// Basic spam check
		$isSpam = 0;
		if ( !empty($rData['lName']) ) {
			$isSpam = 1;
		}

		// Save in database

		// Extra parameters
		$extraData = [
			'lName' => $rData['lName']
		];
		$extraDataStr = \snkeng\core\engine\mysql::real_escape_string(json_encode($extraData, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
		$userAgent = substr(\snkeng\core\engine\mysql::real_escape_string($_SERVER['HTTP_USER_AGENT']), 0, 240);
		$rData['ref'] = urldecode($rData['ref']);

		//
		$ins_qry = <<<SQL
INSERT
INTO sc_site_contact (
	uc_usr_uname, uc_usr_email,
	uc_usr_message, uc_usr_metadata,
	uc_usr_reference,
	uc_usr_ip, uc_usr_useragent,
	uc_status_spam)
VALUES (
	"{$rData['uName']}", "{$rData['eMail']}",
	"{$rData['uMsg']}", "{$extraDataStr}",
	"{$rData['ref']}",
	INET6_ATON('{$_SERVER['REMOTE_ADDR']}'), "{$userAgent}",
	"{$isSpam}"
);
SQL;
		\snkeng\core\engine\mysql::submitQuery($ins_qry, [
			'errorKey' => 'contactSubmitData',
			'errorDesc' => "Unable to save contact request."
		]);

		// Send mail
		if ( !$isSpam ) {
			// Remove slashes for email format
			$rData['uMsg'] = stripcslashes($rData['uMsg']);


			// Datos
			$to = "Contacto <{$_ENV['SE_MAIL_CONTACT']}>"; // Caso especial
			$title = 'Contacto - ' . $siteVars['site']['name'];
			$rData['ref'] = urldecode($rData['ref']);
			$message = <<<TEXT
NOMBRE: {$rData['uName']}
E-MAIL: {$rData['eMail']}
URL Anterior: {$rData['ref']}
MENSAJE:
----
{$rData['uMsg']}
----
TEXT;

			// Operaciones
			$mail = \snkeng\core\general\mail::sendText($to, $title, $message, [
				'replyTo' => $rData['eMail']
			]);

			//
			$response['debug']['mail'] = $mail['debug']['mail'];
		}

		// Exitoso
		$response['s']['d'] = 'Information recieved';
		$response['s']['ex'] = '';
		break;

	//
	default:
		\snkeng\core\engine\nav::invalidPage();
		break;
}
//
