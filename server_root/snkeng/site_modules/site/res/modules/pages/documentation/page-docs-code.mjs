//
import prismExecute from '../../library/prism/prism.mjs';

//
customElements.define('site-page-docs-code', class extends HTMLElement {
	//
	constructor() {
		super();
	}

	//
	connectedCallback() {
		prismExecute(this, false, null);
	}
});
