//
customElements.define(
	'site-page-blog-post',
	class extends HTMLElement {
		urlAjax;
		//
		constructor() {
			super();
		}

		connectedCallback() {
			this.urlAjax = this.dataset['urlApi'];

			// Bindings
			this.addEventListener('click', this.onClickCallBacks.bind(this));
		}

		//
		onClickCallBacks(e) {
			let cBtn = e.target.closest('[data-onclick]');
			if ( !cBtn ) { return; }

			let operation = cBtn.dataset['onclick'];

			//
			if ( typeof this[operation] !== 'function' ) {
				console.error("operación no definida.", operation, cBtn);
				return;
			}

			//
			this[operation](e, cBtn);
		}

		//
		async like(e, cBtn) {
			e.preventDefault();

			let formData = new FormData();
			formData.append( "vote", cBtn.dataset['type'] );

			//
			let fResponse = await fetch(
				this.urlAjax + '/like',
				{
					method: 'POST',
					body: formData,
				}
			);

			let msg = await fResponse.json();

			if ( msg.s.t ) {
				this.querySelector('div[se-elem="postLike"]').innerHTML = msg.d;
			} else {

			}
		}
	}
);
