<?php
//
function docs_get_permits_kill($docId, $docPublished, $docLevel, $docListReq) {
	//
	$permitData = [
		'id' => 0,
		'lastPage' => 0,
		'dStatus' => 0,
	];

	// Public documentation?
	if ( $docLevel === 4 ) { return $permitData; }

	// General level check
	$userLevel = \snkeng\core\engine\login::getLoginLevel();
	if ( $userLevel > $docLevel ) {
		se_killWithError('Permisos insuficientes', "No es posible accesar a la documentación actual (nivel).", 401);
	}

	//
	if ( !$docPublished && $userLevel > 1) {
		se_killWithError('Permisos insuficientes', "No es posible accesar a la documentación actual (publicación).", 401);
	}

	// Doc list requirement
	if ( $docListReq && $userLevel > 1 ) {
		$userId = \snkeng\core\engine\login::getUserId();
		$sql_qry = <<<SQL
SELECT
dacc_id AS id, dacc_last_page AS lastPage, dacc_status AS dStatus
FROM sc_site_documents_access
WHERE doc_id={$docId} AND user_id={$userId}
LIMIT 1;
SQL;
		$permitData = \snkeng\core\engine\mysql::singleRowAssoc($sql_qry, [
			'int' => ['id', 'lastPage', 'dStatus']
		]);

		//
		if ( empty($permitData) ) {
			se_killWithError('Permisos insuficientes', "No es posible accesar a la documentación actual (sin relación).", 401);
		}

	}

	//
	return $permitData;
}