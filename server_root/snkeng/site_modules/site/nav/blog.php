<?php
//
$nav = [];
$GLOBALS['conditions']['app']['site']['settings']['blog']['comments'] =
	( isset($GLOBALS['conditions']['app']['site']['settings']['blog']['comments']) ) ? $GLOBALS['conditions']['app']['site']['settings']['blog']['comments'] : 0;

if ( $GLOBALS['conditions']['app']['site']['settings']['blog']['comments'] === 1 )
{
	$nav['sel'] = "art.art_id AS id, art.art_title AS title, art.art_urltitle AS urltitle, art.art_url AS urlFull,
					art.art_dt_pub AS dtpub, DATE_FORMAT(art.art_dt_pub, '%d %b %Y') AS dtpubf,
					(CASE
					WHEN now() > art.art_dt_pub && art.art_status_published=1 THEN 0
					WHEN now() < art.art_dt_pub && art.art_status_published=1 THEN 1
					ELSE 2
					END) AS published,
					art.art_title AS ptitle, art.art_urltitle AS purltitle,
					art.art_meta_desc AS mincontent,
					art.art_img_struct AS image,
					t2.cat_title AS ctitle, t2.cat_urltitle AS curlname,
					t5.a_obj_name AS bname, t5.a_url_name_full AS burlname,
					t4.com_countmsgs AS msgcount";
	$nav['from'] = 'sc_site_articles AS art
					INNER JOIN sc_site_category AS cat ON cat.cat_id=art.cat_id
					LEFT OUTER JOIN st_socnet_comment_obj AS t4 ON t4.com_id=art.com_id
					LEFT OUTER JOIN sb_objects_obj AS t5 ON t5.a_id=art.a_id';
} else {
	$nav['sel'] = "art.art_id AS pid,
					art.art_title AS ptitle, art.art_urltitle AS purltitle, art.art_url AS urlFull,
					DATE_FORMAT(art.art_dt_pub, '%Y-%m-%dT%TZ') AS dtpubiso, DATE_FORMAT(art.art_dt_pub, '%d %b %Y') AS dtpub,
					(CASE
					WHEN now() > art.art_dt_pub && art.art_status_published=1 THEN 0
					WHEN now() < art.art_dt_pub && art.art_status_published=1 THEN 1
					ELSE 2
					END) AS published,
					art.art_meta_desc AS mincontent, art.art_img_struct AS image,
					cat.cat_title AS ctitle, cat.cat_urltitle AS curlname,
					user.a_obj_name AS user_name, user.a_url_name_full AS user_urlname";
	$nav['from'] = 'sc_site_articles AS art
					INNER JOIN sc_site_category AS cat ON cat.cat_id=art.cat_id
					LEFT OUTER JOIN sb_objects_obj AS user ON user.a_id=art.a_id';
}

//
$nav['lim'] = 12;

// Possible filters
$nav['where'] =  [
	'type'      => ['name' => 'Type', 'db' => 'art.art_type_primary', 'vtype' => 'str', 'stype' => 'eq', 'set'=>'blog'],
	'catId'     => ['name' => 'Category', 'db' => 'art.cat_id', 'vtype' => 'int', 'stype' => 'eq'],
	'author'    => ['name' => 'Author', 'db' => 'art.a_id', 'vtype' => 'int', 'stype' => 'eq'],
	'search'    => ['name' => 'Search', 'db' => 'art.art_title, art.art_meta_desc, art.art_content', 'vtype' => 'str', 'stype' => 'match'],
];

//
if ( \snkeng\core\engine\login::check_loginLevel('admin') ) {
	// Admin
	$nav['order']['s_artDtPub'] = ['name'=>'Publicación', 'db'=>'art.art_dt_pub', 'set'=>'IS NULL DESC'];
	$nav['order']['s_artDtPub_2'] = ['name'=>'Publicación', 'db'=>'art.art_dt_pub', 'set'=>'DESC'];
} else {
	// No admin

	// Filtrar publicados
	$nav['where']['artPublished'] = ['name' => 'Publicado', 'db' => 'art.art_status_published', 'vtype' => 'int', 'stype' => 'eq', 'set' => 1];
	$nav['where']['artDtPublished'] = ['name' => 'Fecha publicación', 'db' => 'art.art_dt_pub', 'vtype' =>'date', 'stype' =>'lte', 'set' => 'NOW()'];
	//
	$nav['order']['s_artDtPub'] = ['name'=>'Publicación', 'db'=>'art.art_dt_pub', 'set'=>'DESC'];
}

//
return $nav;
