<?php

use \snkeng\core\engine\login;

$params = [];
$params['title'] = 'Example';
$params['menu'] = <<<HTML
	<div class="nav3_menu_parent">
		<div class="nav3_menu_button"><a se-nav="app_content" href="{$appData['url']}">Inicio</a></div>
	</div>
	<div class="nav3_menu_parent">
		<div class="nav3_menu_button"><span class="text">Main category</span></div>
		<div class="nav3_menu_hidden">
			<a se-nav="app_content" href="{$appData['url']}/example/">First page</a>
		</div>
	</div>\n
HTML;

if ( \snkeng\core\engine\login::check_loginLevel('sadmin') ) {
	$params['menu'].= <<<HTML
	<div class="nav3_menu_parent">
		<div class="nav3_menu_button"><span class="text">S. Admin.</span></div>
		<div class="nav3_menu_hidden">
		</div>
	</div>\n
HTML;
}

if ( \snkeng\core\engine\login::check_loginLevel('uadmin') ) {
	$params['menu'].= <<<HTML
	<div class="nav3_menu_parent">
		<div class="nav3_menu_button"><span class="text">U. Admin.</span></div>
		<div class="nav3_menu_hidden">
			<a se-nav="app_content" href="{$appData['url']}/uadmin/">Empty</a>
		</div>
	</div>\n
HTML;
}
return $params;
