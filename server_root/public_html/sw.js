'use strict';
//
const sw_config = {
	cacheName: 'v04',
	// Items
	itemsPreCached: [],
	cacheStrat: {
		// Get once, send cache allways. Use: hard coded assets, minified with custom urls, minified CSS, JS
		cacheFirst: [
			'*.ico'
		],
		// Send cached file, check for updates for speed. Use: non unique files (images)
		staleWhileRevalidate: [
			'/res/image/*',
			'/res/svg/*'
		],
		// Not cached by service worker, cache decided in normal cache (usually not cached?)
		networkOnly: [
			'/api/*',
			'/system/*',
			'/server/*',
		],
		// Default behaviour
		networkFirst: [
			// Debuging files should soft cache
			'/res/script/*',
			'/res/modules/*',
			'/se_site_core/**/*.{js,jsm,css,jpg,webp,avif,svg}',
			'/se_core/**/*.{js,jsm,css,jpg,webp,avif,svg}'
		],
	},
	regCache: {
		cacheFirst: [],
		staleWhileRevalidate: [],
		networkOnly: [],
		networkFirst: [],
	},
	offlineImage: '<svg role="img" aria-labelledby="offline-title"'
		+ ' viewBox="0 0 400 300" xmlns="http://www.w3.org/2000/svg">'
		+ '<title id="offline-title">Offline</title>'
		+ '<g fill="none" fill-rule="evenodd"><path fill="#D8D8D8" d="M0 0h400v300H0z"/>'
		+ '<text fill="#9B9B9B" font-family="Times New Roman,Times,serif" font-size="72" font-weight="bold">'
		+ '<tspan x="93" y="172">offline</tspan></text></g></svg>',
	offlinePage: '/offline/'
};

// From https://github.com/fitzgen/glob-to-regexp
function globToRegex(glob, opts) {
	if ( typeof glob !== 'string' ) {
		throw new TypeError('Expected a string');
	}

	var str = String(glob);

	// The regexp we are building, as a string.
	var reStr = "";

	// Whether we are matching so called "extended" globs (like bash) and should
	// support single character matching, matching ranges of characters, group
	// matching, etc.
	var extended = opts ? !!opts.extended : false;

	// When globstar is _false_ (default), '/foo/*' is translated a regexp like
	// '^\/foo\/.*$' which will match any string beginning with '/foo/'
	// When globstar is _true_, '/foo/*' is translated to regexp like
	// '^\/foo\/[^/]*$' which will match any string beginning with '/foo/' BUT
	// which does not have a '/' to the right of it.
	// E.g. with '/foo/*' these will match: '/foo/bar', '/foo/bar.txt' but
	// these will not '/foo/bar/baz', '/foo/bar/baz.txt'
	// Lastely, when globstar is _true_, '/foo/**' is equivelant to '/foo/*' when
	// globstar is _false_
	var globstar = opts ? !!opts.globstar : false;

	// If we are doing extended matching, this boolean is true when we are inside
	// a group (eg {*.html,*.js}), and false otherwise.
	var inGroup = false;

	// RegExp flags (eg "i" ) to pass in to RegExp constructor.
	var flags = opts && typeof (opts.flags) === "string" ? opts.flags : "";

	var c;
	for ( var i = 0, len = str.length; i < len; i++ ) {
		c = str[i];

		switch ( c ) {
			case "/":
			case "$":
			case "^":
			case "+":
			case ".":
			case "(":
			case ")":
			case "=":
			case "!":
			case "|":
				reStr += "\\" + c;
				break;

			case "?":
				if ( extended ) {
					reStr += ".";
					break;
				}

			case "[":
			case "]":
				if ( extended ) {
					reStr += c;
					break;
				}

			case "{":
				if ( extended ) {
					inGroup = true;
					reStr += "(";
					break;
				}

			case "}":
				if ( extended ) {
					inGroup = false;
					reStr += ")";
					break;
				}

			case ",":
				if ( inGroup ) {
					reStr += "|";
					break;
				}
				reStr += "\\" + c;
				break;

			case "*":
				// Move over all consecutive "*"'s.
				// Also store the previous and next characters
				var prevChar = str[i - 1];
				var starCount = 1;
				while ( str[i + 1] === "*" ) {
					starCount++;
					i++;
				}
				var nextChar = str[i + 1];

				if ( !globstar ) {
					// globstar is disabled, so treat any number of "*" as one
					reStr += ".*";
				} else {
					// globstar is enabled, so determine if this is a globstar segment
					var isGlobstar = starCount > 1                      // multiple "*"'s
						&& (prevChar === "/" || prevChar === undefined)   // from the start of the segment
						&& (nextChar === "/" || nextChar === undefined)   // to the end of the segment

					if ( isGlobstar ) {
						// it's a globstar, so match zero or more path segments
						reStr += "((?:[^/]*(?:\/|$))*)";
						i++; // move over the "/"
					} else {
						// it's not a globstar, so only match one path segment
						reStr += "([^/]*)";
					}
				}
				break;

			default:
				reStr += c;
		}
	}

	// When regexp 'g' flag is specified don't
	// constrain the regular expression with ^ & $
	if ( !flags || !~flags.indexOf('g') ) {
		reStr = "^" + reStr + "$";
	}

	return new RegExp(reStr, flags);
};

//
function convertAll() {
	//
	for ( let cListTitle in sw_config.cacheStrat ) {
		for ( let cElement of sw_config.cacheStrat[cListTitle] ) {
			sw_config.regCache[cListTitle].push(globToRegex(cElement, {extended: true}));
		}
	}

	//
}

//
convertAll();

//
self.addEventListener('install', event => {
	function onInstall(event, opts) {
		return caches.open(cListTitle + '_' + sw_config.cacheName).then(cache => cache.addAll(opts.staticCacheItems));
	}

	if ( !sw_config.itemsPreCached.length ) {
		console.log("SW, nothing pre cached.");
		return;
	}

	//
	event.waitUntil(
		onInstall(event, sw_config).then(() => self.skipWaiting())
	);
});

//
self.addEventListener('activate', event => {
	function onActivate(event, opts) {
		return caches.keys()
			.then(cacheKeys => {
				let oldCacheKeys = cacheKeys.filter(key => key.indexOf(opts.version) !== 0),
					deletePromises = oldCacheKeys.map(oldKey => caches.delete(oldKey));
				return Promise.all(deletePromises);
			});
	}

	event.waitUntil(
		onActivate(event, sw_config).then(() => self.clients.claim())
	);
});

// Cache validator
self.addEventListener('fetch', (event) => {
	let request = event.request,
		requestUrl = new URL(request.url);

	//
	if ( self.location.origin !== requestUrl.origin || request.method !== 'GET' ) {
		return;
	}

	// Search in each cache strategy
	search_loop:
		for ( let cListTitle in sw_config.regCache ) {
			//
			for ( let cElement of sw_config.regCache[cListTitle] ) {
				if ( cElement.test(requestUrl.pathname) ) {
					//
					switch ( cListTitle ) {
						//
						case 'cacheFirst':
							event.respondWith(caches.open(cListTitle + '_' + sw_config.cacheName).then((cache) => {
								// Go to the cache first
								return cache.match(event.request.url).then((cachedResponse) => {
									// Return a cached response if we have one
									if ( cachedResponse ) {
										return cachedResponse;
									}

									// Otherwise, hit the network
									return fetch(event.request).then((fetchedResponse) => {
										// Add the network response to the cache for later visits
										cache.put(event.request, fetchedResponse.clone());

										// Return the network response
										return fetchedResponse;
									});
								});
							}));
							return;
						//
						case 'staleWhileRevalidate':
							event.respondWith(caches.open(cListTitle + '_' + sw_config.cacheName).then((cache) => {
								return cache.match(event.request).then((cachedResponse) => {
									const fetchedResponse = fetch(event.request).then((networkResponse) => {
										cache.put(event.request, networkResponse.clone());
										return networkResponse;
									});

									return cachedResponse || fetchedResponse;
								});
							}));
							return;
						//
						case 'networkFirst':
							break search_loop;
						// Skips all internal operations
						case 'networkOnly':
							return;
						//
						default:
							console.error("SW - Cache definition error in:", requestUrl.pathname);
							break;
					}
				}
			}
		}

	// Open the cache
	event.respondWith(caches.open('networkFirst_' + sw_config.cacheName).then((cache) => {
		// Go to the network first
		return fetch(event.request.url).then((fetchedResponse) => {
			cache.put(event.request, fetchedResponse.clone());

			return fetchedResponse;
		}).catch(() => {
			// If the network is unavailable, get
			return cache.match(event.request.url);
		});
	}));
});
//